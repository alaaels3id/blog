<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Back\AdminController;
use App\Http\Controllers\Back\ContactController;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Back\RoleController;
use App\Http\Controllers\Back\SettingController;
use App\Http\Controllers\Back\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Back\Auth\AdminLoginController;

Route::namespace('Auth')->group(function (){
    # Admin Login
    Route::get('/login', [AdminLoginController::class, 'showAdminLoginForm'])->name('admin.login');
    Route::post('/login', [AdminLoginController::class, 'adminLogin'])->name('admin.submit.login');
    Route::post('/logout', [AdminLoginController::class, 'adminLogout'])->name('admin.logout');
});

# Admin routes
Route::group(['middleware' => ['auth:admin', 'CheckAdminRole', 'CheckAdminStatus']], function () {
    // Admin -> home page
    Route::get('/', [DashboardController::class, 'index'])->middleware('SmtpConfigration')->name('admin-panel');

    // admin -> roles Routes
    Route::get('roles/index', [RoleController::class, 'apiIndex'])->name('roles.apiIndex');
    Route::resource('roles', RoleController::class, ['except' => ['show', 'destroy']]);
    crudRoutes('roles', RoleController::class);

    // admin -> users Routes
    Route::get('users/index', [UserController::class, 'apiIndex'])->name('users.apiIndex');
    Route::resource('users', UserController::class, ['except' => ['show', 'destroy']]);
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::post('/ajax-delete-user', [UserController::class, 'DeleteUser'])->name('ajax-delete-user');
        Route::post('/ajax-change-user-status', [UserController::class, 'ChangeStatus'])->name('ajax-change-user-status');
        Route::get('/show/{user}', [UserController::class, 'show'])->name('show-user');
        Route::get('/export', [UserController::class, 'export'])->name('export');
        Route::get('/{id}/show-user-message', [UserController::class, 'showUserMessage'])->name('show.message');
        Route::post('/send-user-message', [UserController::class, 'sendUserMessage'])->name('send.message');
        Route::get('/get/trashed', [UserController::class, 'trashed'])->name('trashed');
        Route::get('/restore/{id}/trashed', [UserController::class, 'restore'])->name('restore');
        Route::get('/delete/{id}/trashed', [UserController::class, 'forceDelete'])->name('delete');
        Route::post('/send-user-notification', [UserController::class, 'sendUserNotification'])->name('send-notification');
        Route::get('/{user}/user-performance-report', [UserController::class, 'userPerformanceReport'])->name('user-performance-report');
    });

    // admin -> Settings Routes
    Route::resource('settings', SettingController::class, ['except' => ['show', 'destroy']]);
    Route::get('settings/exports', [SettingController::class, 'export'])->name('settings.export');
    Route::post('settings/update-all', [SettingController::class, 'UpdateAll'])->name('settings.update-all');

    // admin -> Admins Routes
    Route::get('admins/index', [AdminController::class, 'apiIndex'])->name('admins.apiIndex');
    Route::resource('admins', AdminController::class, ['except' => ['show', 'destroy']]);
    Route::group(['prefix' => 'admins', 'as' => 'admins.'], function () {
        Route::get('/admin/profile', [AdminController::class, 'adminProfile'])->name('profile');
        Route::post('/admin/profile', [AdminController::class, 'AdminUpdateProfile'])->name('admin-profile-update');
        Route::post('/ajax-delete-admin', [AdminController::class, 'DeleteAdmin'])->name('ajax-delete-admin');
        Route::post('/ajax-change-admin-status', [AdminController::class, 'ChangeStatus'])->name('ajax-change-admin-status');
        Route::get('/show/{admin}', [AdminController::class, 'showAdmin'])->name('show-admin');
        Route::get('/{id}/show-admin-message', [AdminController::class, 'showAdminMessage'])->name('show.message');
        Route::post('/send-admin-message', [AdminController::class, 'sendAdminMessage'])->name('send.message');
        Route::get('/export', [AdminController::class, 'export'])->name('export');
        Route::get('/get/trashed', [AdminController::class, 'trashed'])->name('trashed');
        Route::get('/restore/{id}/trashed', [AdminController::class, 'restore'])->name('restore');
        Route::get('/delete/{id}/trashed', [AdminController::class, 'forceDelete'])->name('delete');
    });

    // admin -> Contacts Routes
    Route::group(['prefix' => 'contacts', 'as' => 'contacts.'], function () {
        Route::get('/', [ContactController::class, 'Contacts'])->name('index');
        Route::post('/ajax-delete-contact', [ContactController::class, 'DeleteContact'])->name('ajax-delete-contact');
        Route::get('/{id}/message-details', [ContactController::class, 'showMessageDetails'])->name('message.details');
        Route::get('/{id}/send-message', [ContactController::class, 'sendUserMessage'])->name('send-user-message');
        Route::post('/users-send-email', [ContactController::class, 'SendUserReplayMessage'])->name('users-send-email');
        Route::get('/export', [ContactController::class, 'ContactsExport'])->name('export');
    });
});
