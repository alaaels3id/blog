@extends('Back.layouts.master')

@section('title', trans('back.'.str()->plural($model)))

@section('style')
    {{ $styles ?? '' }}
@stop

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <x-page-title :title="trans('back.'.str()->plural($model))"></x-page-title>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.'.str()->plural($model))</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        <div class="panel-heading">
            @include('Back.includes.table-header', ['name' => str()->plural($model)])
        </div>
        {{ $slot }}
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
    @include('Back.includes.isChecked', ['model' => $model])
    @include('Back.includes.deleteActionScript', ['model' => $model, 'modelType' => $modelType])
    {{ $scripts ?? '' }}
@stop
