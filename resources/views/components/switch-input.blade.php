<div class="checkbox checkbox-switchery switchery-lg">
    <label>
        @php
            $status = isset($model) ? ($model->status == 1 ? true : false) : true;
        @endphp

        {!! Form::checkbox('status',1, $status,['class'=>'form-control switchery form-data','id' => 'status']) !!}

        @lang('back.form-status')
    </label>
</div>
