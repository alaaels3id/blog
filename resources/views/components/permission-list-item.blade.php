<li class="list-group-item">
    <div class="checkbox checkbox-switchery checkbox-right switchery-lg" style="font-size: 22px;">
        <label style="font-size: x-large;">
            <input
                type="checkbox"
                @isset($role) {{ edit_permissions($role->permissions, $val) }} @endisset
                class="form-control form-data switchery"
                value="{{$val}}"
                name="permissions[]"
                id="permissions_{{$k}}">
        </label>
        @lang('crud.' . $val)
    </div>
</li>
