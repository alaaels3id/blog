<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

        <ul class="dropdown-menu dropdown-menu-{{ floating('right', 'left') }}">
            <li>
                <a href="{{ route($table.'.restore', ['id' => $model->id]) }}">
                    <i class="icon-database-edit2"></i>@lang('back.restore')
                </a>
            </li>
            <li>
                <a href="{{ route($table.'.delete', ['id' => $model->id]) }}">
                    <i class="icon-database-remove"></i>@lang('back.delete')
                </a>
            </li>
        </ul>
    </li>
</ul>
