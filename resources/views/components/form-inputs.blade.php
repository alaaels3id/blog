@if($type == 'password_confirmation')

    @include('Back.includes.inputs', [
        'type'   => 'password_confirmation',
        'name'   => 'password',
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-password-confirm'),
    ])

@elseif($type == 'image')

    @include('Back.includes.inputs', [
        'type'   => 'image',
        'name'   => $name,
        'style'  => 'form-control form-data',
        'col'    => '6',
        'slug'   => $slug,
    ])

@elseif($type == 'file')

    @include('Back.includes.inputs', [
        'type'   => 'file',
        'name'   => $name,
        'style'  => 'form-control form-data',
        'col'    => '6',
        'slug'   => trans('back.form-file'),
    ])

@elseif($type == 'ckeditor')
    <textarea class="form-data editorfull" name="{{$name}}" id="editorfullar" rows="4" cols="4"></textarea>
@else
    @include('Back.includes.inputs', [
        'type'   => $type,
        'name'   => $name,
        'style'  => 'form-control form-data',
        'slug'   => $slug,
    ])
@endif
