@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">title</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>body</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$post->id}}</td>
                                    <td>{{ ucwords($post->title) }}</td>
                                    <td>{{ ucwords($post->body) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
