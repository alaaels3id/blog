@extends('Back.layouts.master')

@section('title', $user->name)

@section('content')
    <x-page-show-header model="user" :title="$user->name"></x-page-show-header>

    <div class="content">
        <div class="row">
            @include('includes.flash')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-flat border-top-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3" style="float: {{ floating('right', 'left') }}">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img style="width: 300px;height: 200px;" src="{{ $user->image_url }}" alt="">
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info btn-sm">@lang('back.edit')</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="text-semibold no-margin-top" title="{{ $user->name }}">
                                            {{ ucwords(str()->limit($user->name, 30, '...')) }}
                                        </h6>
                                    </div>
                                </div>
                                <div class="lead">
                                    <button class="btn btn-success btn-block" data-toggle="modal" data-target="#view_show_user_send_notification">@lang('back.send-notification')
                                        <i class="icon-bell-check"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-9" style="float: {{ floating('left', 'right') }}">
                                <div class="well">
                                    <dl dir="{{ direction() }}">
                                        <dt>@lang('back.form-email')</dt>
                                        <dd>{{ $user->email ?? trans('back.no-value') }}</dd>

                                        <dt>@lang('back.form-phone')</dt>
                                        <dd>{{ $user->mobile ?? trans('back.no-value') }}</dd>

                                        <dt>@lang('back.form-status')</dt>
                                        <dd>
                                            @if($user->status == 1) <span class="label label-success">@lang('back.active')</span>
                                            @else <span class="label label-danger">@lang('back.disactive')</span>
                                            @endif
                                        </dd>

                                        <dt>@lang('back.since')</dt>
                                        <dd>{{ $user->created_at->diffForHumans() }}</dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="view_show_user_send_notification" class="modal fade">
        <div class="modal-dialog">
            <form action="{{ route('users.send-notification') }}" method="post">
                <div class="modal-content" dir="{{direction()}}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h1 class="modal-title">@lang('back.send-a-message')</h1>
                    </div>

                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="form-group @error('title') has-error @enderror">
                            <label for="title">@lang('back.form-title')</label>
                            <input type="text" name="title" id="title" class="form-control">
                            @error('title') <span
                                style="color: #d84315"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                        <div class="form-group @error('title') has-error @enderror">
                            <label for="body">@lang('back.form-body')</label>
                            <input type="text" name="body" id="body" class="form-control">
                            @error('body') <span style="color: #d84315"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">@lang('back.close')</button>
                        <button type="submit" name="submit" class="btn btn-primary">@lang('back.send')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
