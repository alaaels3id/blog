@extends('Back.layouts.master')

@section('title', $admin->name)

@section('content')
    <x-page-show-header model="admin" :title="$admin->name"></x-page-show-header>

    <div class="content">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2">
                <!-- Basic table -->
                <div class="panel panel-primary">
                    <div class="panel-heading">@lang('back.admin')</div>
                    <div class="panel-body">
                    	<div class="row">
                    		<div class="col-md-3" style="float: {{ floating('right', 'left') }}">
                    			<div class="thumbnail">
    								<div class="thumb">
                                        <img style="width: 300px;height: 200px;" src="{{ $admin->image_url }}" alt="">
    									<div class="caption-overflow">
    										<span>
    											<a href="{{ route('admins.edit', $admin->id) }}" class="btn btn-info btn-sm">
                                                    @lang('back.edit')
                                                </a>
    										</span>
    									</div>
    								</div>

    								<div class="caption">
    									<h6 class="text-semibold no-margin-top" title="{{ $admin->name }}">
    										{{ ucwords(str()->limit($admin->name, 30, '...')) }}
    									</h6>
    								</div>
    							</div>
                    		</div>
                    		<div class="col-md-9" style="float: {{ floating('left', 'right') }}">
								<div class="well">
				                    <dl dir="{{ direction() }}">
										<dt>@lang('back.form-email')</dt>
										<dd><a href="mailto:{{ $admin->email }}">{{ $admin->email }}</a></dd>

										<dt>@lang('back.form-phone')</dt>
										<dd>{{ $admin->phone ?? trans('back.no-value')  }}</dd>

										<dt>@lang('back.form-status')</dt>

										<dd>@include('Back.includes.is-active', ['status' => $admin->status])</dd>

										<dt>@lang('back.since')</dt>
										<dd>{{ $admin->created_at->diffForHumans() }}</dd>
									</dl>
								</div>
                    		</div>
                    	</div>
                    </div>
                </div>
                <!-- /basic table -->
    		</div>
    	</div>
    </div>
@stop
