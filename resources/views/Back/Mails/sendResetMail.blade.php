@component('mail::message')
# Introduction

Your reset code is {{$token}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
