@extends('Back.layouts.master')

@section('title', trans('back.home'))

@section('style')
    <style>
        .custum-label {
            font-size: 15px;
            border-color: cadetblue;
            background-color: cadetblue;
        }

        @media (min-width: 1025px) {
            .colmd2 {
                width: 13.666667%;
            }
        }
    </style>
@stop

@section('content')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;"
                       class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span> - @lang('back.dashboard')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right', 'left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li class="active">@lang('back.dashboard')</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-xs-12" dir="{{ direction() }}">
                <div class="row">
                    @foreach (models() as $model => $option)
                        <div class="col-md-3 col-xs-12 col-md-6" style="float: {{ floating('right', 'left') }};">
                            <div class="panel bg-{{ $option['color'] }}">
                                <div class="panel-body">
                                    <h3 class="no-margin">{{ getModelCount($model) ?? 0 }}</h3>
                                    @lang('back.'.str()->plural($model))
                                </div>

                                <div class="container-fluid">
                                    <div class="chart" id="members-online"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

{{--            <div class="col-lg-12">--}}
{{--                <div class="panel panel-flat">--}}
{{--                    <div class="panel-heading">--}}
{{--                        <h5 class="panel-title">Chart for users</h5>--}}
{{--                        <div class="heading-elements">--}}
{{--                            <ul class="icons-list">--}}
{{--                                <li><a data-action="collapse"></a></li>--}}
{{--                                <li><a data-action="reload"></a></li>--}}
{{--                                <li><a data-action="close"></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="panel-body">--}}
{{--                        <div class="chart-container">--}}
{{--                            <div class="chart" id="google-line"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Latest users -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h6 class="panel-title">@lang('back.latest-users')</h6>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                                <li><a data-action="close"></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="row">
                                            @forelse($latest_users->chunk(2) as $chunk)
                                                <div class="col-lg-6">
                                                    <ul class="media-list content-group">
                                                        @foreach($chunk as $user)
                                                            <li class="media stack-media-on-mobile">
                                                                <div class="media-left">
                                                                    <div class="thumb">
                                                                        <a href="{{ route('users.show-user', $user->id ?? 0) }}">
                                                                            <img src="{{ $user->image_url ?? defaultImage() }}" style="width: 90px;height: 90px;" class="img-responsive img-rounded media-preview" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <div class="media-body">
                                                                    <h6 class="media-heading"><a href="{{ route('users.show-user', $user->id ?? 0) }}">{{ ucwords($user->name ?? trans('back.no-value')) }}</a></h6>
                                                                    <ul class="list-inline list-inline-separate text-muted mb-5">
                                                                        <li><i class="icon-user position-left"></i> {{ $user->mobile ?? trans('back.no-value') }}</li>
                                                                        <li>{{ $user->created_at->diffForHumans() ?? trans('back.no-value') }}</li>
                                                                    </ul>
                                                                    {{ $user->email ?? trans('back.no-value') }}
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @empty
                                                <div class="alert alert-info text-center">@lang('back.no-value')</div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <!-- /latest posts -->
                            </div>

{{--                            <div class="col-lg-12">--}}
{{--                                <div class="panel panel-flat">--}}
{{--                                    <div class="panel-heading">--}}
{{--                                        <h6 class="panel-title">@lang('back.users')</h6>--}}
{{--                                    </div>--}}

{{--                                    <div class="container-fluid">--}}
{{--                                        <div class="row text-center">--}}
{{--                                            @foreach($days as $index => $day)--}}
{{--                                                <div class="colmd2 col-md-2">--}}
{{--                                                    <div class="content-group">--}}
{{--                                                        <h6 class="text-semibold no-margin"><i class="icon-clipboard3 position-left text-slate"></i> {{ $day->count() }}</h6>--}}
{{--                                                        <span class="text-muted text-size-small">@lang('back.days.day-var',['var' => trans('back.days.'.$index)])</span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="chart" id="messages-stats"></div>--}}

{{--                                    <ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">--}}
{{--                                        @foreach($days as $index => $day)--}}
{{--                                            <li {{ $loop->first ? 'class="active"' : '' }}>--}}
{{--                                                <a href="#messages-{{$index}}" class="text-size-small text-uppercase" data-toggle="tab">--}}
{{--                                                    @lang('back.days.'.$index)--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}

{{--                                    <div class="tab-content">--}}
{{--                                        @foreach($days as $index => $day)--}}
{{--                                            <div class="tab-pane {{ $loop->first ? 'active' : '' }} fade in has-padding" id="messages-{{$index}}">--}}
{{--                                                <ul class="media-list">--}}
{{--                                                    @forelse($day->take(12)->shuffle() as $user)--}}
{{--                                                        <li class="media">--}}
{{--                                                            <div class="media-left">--}}
{{--                                                                <img src="{{ $user->image_url ?? defaultImage() }}" class="img-circle img-xs" alt="">--}}
{{--                                                            </div>--}}

{{--                                                            <div class="media-body">--}}
{{--                                                                <a href="{{ route('users.show-user',$user->id ?? 0) }}">--}}
{{--                                                                    {{ ucwords($user->name ?? trans('back.no-value')) }}--}}
{{--                                                                    <span class="media-annotation pull-right">{{ $user->created_at->format('H:i')  ?? trans('back.no-value') }}</span>--}}
{{--                                                                </a>--}}

{{--                                                                <span class="display-block text-muted">{{ $user->email ?? trans('back.no-value') }}</span>--}}
{{--                                                            </div>--}}
{{--                                                        </li>--}}
{{--                                                    @empty--}}
{{--                                                        <li class="alert alert-info text-center">@lang('back.no-value')</li>--}}
{{--                                                    @endforelse--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">@lang('back.users')</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                @if($users->count() > 0)
                                    <div id="google-donut-exploded-a"></div>
                                @else
                                    <div class="alert alert-info text-center">@lang('back.no-value')</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="col-lg-12">--}}
{{--                <div class="panel panel-flat">--}}
{{--                    <div class="panel-heading">--}}
{{--                        <h5 class="panel-title">@lang('back.users-performance-report')</h5>--}}
{{--                        <div class="heading-elements">--}}
{{--                            <ul class="icons-list">--}}
{{--                                <li><a data-action="collapse"></a></li>--}}
{{--                                <li><a data-action="reload"></a></li>--}}
{{--                                <li><a data-action="close"></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="panel-body">--}}
{{--                        <div class="table-responsive">--}}
{{--                            <table class="table">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th>#</th>--}}
{{--                                    <th>@lang('back.form-name')</th>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}
{{--                                @foreach($users as $index => $user)--}}
{{--                                    <tr>--}}
{{--                                        <td>{{ $index+1 }}</td>--}}
{{--                                        <td>{{ $user->name }}</td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
    <!-- /content area -->
@stop
@section('scripts')
    @include('Back.includes.pieChart', [
        'el'        => 'google-donut-exploded-a',
        'counter'   => 1,
        'active'    => $users->where('status', 1)->count(),
        'disactive' => $users->where('status', 0)->count()
    ])
@stop
