@forelse($projects as $index => $project)
    <tr>
        <td>{{ $index + 1 }}</td>
        <td>{{ $project->name ?? trans('back.no-value') }}</td>
        <td>{{ str_limit_30($project->description ?? trans('back.no-value')) }}</td>
        <td>{{ $project->start_date->format('Y-m-d') ?? trans('back.no-value') }}</td>
        <td>
            <table class="table">
                <thead>
                <tr>
                    <th>@lang('back.form-date')</th>
                    <th>@lang('back.form-completion-rate')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($project->disbursements as $disbursement)
                    <tr>
                        <td>{{ $disbursement->completion_date->format('Y-m-d') }}</td>
                        <td>{{ $disbursement->completion_rate }}%</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </td>
    </tr>
@empty
    <tr>
        <td colspan="5" class="text-center">@lang('back.no-value')</td>
    </tr>
@endforelse
