<td>
    <div class="checkbox checkbox-switchery switchery-lg">
        <label>
            <input
                type="checkbox" onclick="isChecked('{{ $status == 1 ? 'checked' : 'null' }}', '{{ $id }}');"
                id="active-id-{{ $id }}" {{ $status == 1 ? 'checked' : '' }} value="{{ $id }}"
                name="status" class="form-control switchery form-data">
        </label>
    </div>
</td>
