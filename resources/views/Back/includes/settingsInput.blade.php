@if($setting->input == 0)
    <div class="form-group">
        <label for="{{$setting->key}}">{{ucwords($setting->name)}}</label>
        <input type="text" class="form-control form-data" name="{{$setting->key}}" id="{{$setting->key}}" value="{{$setting->value}}">
    </div>
@elseif($setting->input == 2)
    <div class="form-group">
        <div class="col-xs-12">
            <div class="row img-media">
                <div class="col-xs-6">
                    <div class="form-valid">
                        <label for="image">{{ ucwords($setting->name) }}</label><br>
                        <input type="file" class="file-styled form-control form-data" id="{{ $setting->key }}" name="{{ $setting->key }}">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="img-container">
                        @if(isset(explode('.',$setting->value)[1]))
                            @if(explode('.',$setting->value)[1] == 'jpg' || explode('.',$setting->value)[1] == 'png' || explode('.',$setting->value)[1] == 'jpeg')
                                <img id="viewImage" class="img-responsive" width="100" height="100" src="{{ asset('public/uploaded/settings/'.$setting->value) }}" alt=""/>
                            @elseif(isset(explode('.',$setting->value)[1]) && explode('.',$setting->value)[1] == 'pdf')
                                <i class="fa fa-file-pdf-o fa-5x" style="margin: 10px;"></i>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($setting->input == 3)
    <div id="map_{{$setting->key}}" style="width: 100%; height: 400px;"></div>
    <input type="hidden" name="searchmap" id="searchmap">
    <input type="hidden" class="form-control form-data" id="{{ $setting->key }}" name="{{ $setting->key }}" value="{{ $setting->value }}">
    <script>
        var map, marker, input, searchBox;

        let map_input = '{{ $setting->key }}';

        let map_api = "{{ getSetting('map_api') }}";

        let lati = parseFloat("{{ explode(',', $setting->value)[0] }}");

        let lngi = parseFloat("{{ explode(',', $setting->value)[1] }}");

        const place_changed = function() {
            var place = autocomplete.getPlace();

            if (marker && marker.setMap) {
                marker.position = place.geometry.location;
                marker.map = map;
                marker.setPosition(place.geometry.location);
            }

            if (place.geometry.viewport)
            {
                map.fitBounds(place.geometry.viewport);
            }
            else
            {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }

            $('#'+map_input).val(place.geometry.location.lat()+','+place.geometry.location.lng());

            let bounds = new google.maps.LatLanBounds();

            var i , place;

            for (i = 0; place = places[i]; i++) { bounds.extend(place.geometry.Location); } //set marker position new ...

            map.fitBounds(bounds);

            map.setZoom(9);
        }

        var position_changed = function() {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#' + map_input).val(lat+','+lng);
            displayLocation(lat,lng);
        };

        function displayLocation(latitude, longitude)
        {
            var request = new XMLHttpRequest();

            var method = 'GET';

            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude+',' + longitude + "&key=" + map_api + "&language=ar";

            var async = true;

            request.open(method, url, async);

            request.onreadystatechange = function() {
                if(request.readyState == 4 && request.status == 200) {
                    var data = JSON.parse(request.responseText);

                    if(data.status === 'REQUEST_DENIED')
                    {
                        console.log(data.error_message);
                    }
                    else
                    {
                        var address = data.results[0];
                        $('#searchmap').val(address);
                    }
                }
            };
            request.send();
        };

        function initMap()
        {
            map = new google.maps.Map(document.getElementById('map_{{ $setting->key }}'), { center: {lat: lati, lng: lngi }, zoom: 9 });

            marker = new google.maps.Marker({ position: {lat: lati, lng: lngi}, map: map, draggable :true });

            var autocomplete = new google.maps.places.Autocomplete(document.getElementById('searchmap'));

            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', place_changed);

            google.maps.event.addListener(marker,'position_changed', position_changed);
        }
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{getSetting('map_api')}}&libraries=places&callback=initMap"></script>
@else
    <div class="form-group">
        <label for="{{$setting->key}}">{{ucwords($setting->name)}}</label>
        <textarea class="form-control form-data" name="{{$setting->key}}" id="{{$setting->key}}" cols="30" rows="10">{{$setting->value}}</textarea>
    </div>
@endif
