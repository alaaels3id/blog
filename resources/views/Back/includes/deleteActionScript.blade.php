<script>

    let currentTable = '{{ str()->plural($model) }}';
    let currentModel = '{{ $model }}';
    let deleteMessage = '{{ trans('back.delete-message-var', ['var' => trans('back.t-'.$model), 'type' => trans('back.for-'.$modelType)]) }}';
    let deleteMessageTitle = '{{ trans('back.delete-message-title') }}';

    $('a.delete-action').on('click', function (e) {
        var id = $(this).data('id');
        var tbody = $(`table#${currentTable} tbody`);
        var count = tbody.data('count');

        e.preventDefault();

        swal({
            title: deleteMessage,
            text: deleteMessageTitle,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var tbody = $(`table#${currentTable} tbody`);
                    var count = tbody.data('count');

                    $.ajax({
                        type: 'POST',
                        url: '{{ route(str()->plural($model).'.ajax-delete-'.$model) }}',
                        data: {id},
                        success: function (response) {
                            if (response.deleteStatus) {
                                $(`#${currentModel}-row-${id}`).fadeOut();
                                count = count - 1;
                                tbody.attr('data-count', count);
                                swal(response.message, {icon: "success"});
                            } else {
                                swal(response.error);
                            }
                        },
                        error: (x) => crud_handle_server_errors(x),
                        complete: function () {
                            if (count === 1) tbody.append(`<tr><td colspan="5"><strong>No data available in table</strong></td></tr>`);
                        }
                    });
                } else {
                    swal({
                        title: "@lang('back.a-message')",
                        text: "تم إلغاء العملية",
                        icon: "success",
                        button: "حسنا",
                    });
                }
            });
    });
</script>
