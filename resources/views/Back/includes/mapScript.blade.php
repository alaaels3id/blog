<script>
    function initMap()
    {
        let map_id = '{{ $map_id }}';

        let map_input = '{{ $map_input }}';

        let lat_val = $('#' + map_id).data('lat');

        let lng_val = $('#' + map_id).data('lng');

        var map = new google.maps.Map(document.getElementById(map_id), { center: {lat: lat_val, lng: lng_val}, zoom: 15 });

        var input = document.getElementById('searchInput');

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: {lat: lat_val, lng: lng_val},
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable :true
        });

        google.maps.event.addListener(map, 'click', function (event) {
            $('#map_settings_input').val(event.latLng.lat() + ',' + event.latLng.lng());
            //document.getElementById("geo_lat").value = event.latLng.lat();
            //document.getElementById("geo_lng").value = event.latLng.lng();
            marker.setPosition(event.latLng);
        });

        marker.addListener('position_changed', printMarkerLocation);

        function printMarkerLocation() {
            $('#map_settings_input').val(marker.position.lat() + ',' + marker.position.lng());
            //document.getElementById('geo_lat').value = marker.position.lat();
            //document.getElementById('geo_lng').value = marker.position.lng();
        }

        autocomplete.addListener('place_changed', function ()
        {
            infowindow.close();

            marker.setVisible(false);

            var place = autocomplete.getPlace();

            if (!place.geometry)
            {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport)
            {
                map.fitBounds(place.geometry.viewport);
            }
            else
            {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setIcon(({ url: place.icon, size: new google.maps.Size(71, 71), origin: new google.maps.Point(0, 0), anchor: new google.maps.Point(17, 34), scaledSize: new google.maps.Size(35, 35) }));

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#' + map_input).val(place.geometry.location.lat()+','+place.geometry.location.lng());

        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap&key={{ getSetting('map_api') }}" ></script>
