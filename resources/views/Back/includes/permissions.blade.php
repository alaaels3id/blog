<div class="row">
    @php $z=1;$d=2; @endphp
    @foreach($getAllRoutes as $key => $value)
        @if(is_array($value))
            @php $chunks = array_chunk($value, 6); @endphp
            <div class="col-md-6 col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading" style="text-align: center;font-size: 20px;"><b>@lang('crud.' . $key)</b></div>
                    <div class="panel-body" style="height: 630px;">
                        <ul class="list-group" style="height: 580px;">
                            <div class="tabbable">
                                <ul class="nav nav-tabs-alt nav-tabs nav-tabs-solid nav-tabs-component nav-justified">
                                    @foreach($chunks as $ck => $xx)
                                        <li {{ $loop->first ? 'class=active' : '' }}>
                                            <a href="#fields-{{$ck.$z.$d}}" data-toggle="tab">
                                                <strong class="nav-tab-title-js">@lang('back.page-number', ['var' => $ck+1])</strong>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <div class="tab-content">
                                    @foreach($chunks as $ii => $chunk)
                                        <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="fields-{{$ii.$z.$d}}">
                                            @foreach ($chunk as $keys => $field)
                                                @continue(explode('.', $field)[1] == 'apiIndex')
                                                <x-permission-list-item :role="$role" :val="$field" :k="$ii.$z.$d.$keys"></x-permission-list-item>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            @php $z++; @endphp
        @else
            <div class="col-md-6 col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading" style="text-align: center;font-size: 20px;"><b>@lang('crud.' . $value)</b></div>
                    <div class="panel-body" style="height: 580px;">
                        <div class="tabbable">
                            <ul class="nav nav-tabs-alt nav-tabs nav-tabs-solid nav-tabs-component nav-justified">
                                <li class="active">
                                    <a href="#fields-name-1" data-toggle="tab">
                                        <strong class="nav-tab-title-js">@lang('back.page-number', ['var' => 1])</strong>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="fields-name-1">
                                    <x-permission-item :role="$role" :value="$value" :key="$key"></x-permission-item>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>
