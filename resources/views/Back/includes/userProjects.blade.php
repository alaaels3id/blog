@if($user->user_role_id == 1)
    @include('Back.includes.userProjectRowTable', ['projects' => $user->projectsAsManger])
@elseif($user->user_role_id == 2)
    @include('Back.includes.userProjectRowTable', ['projects' => $user->projectsAsLeader])
@elseif($user->user_role_id == 3)
    @include('Back.includes.userProjectRowTable', ['projects' => $user->projectsAsProgramManger])
@endif
