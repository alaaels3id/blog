@forelse($chunk as $i => $type)
    <div class="col-xs-12">
        <div class="panel panel-flat border-top-success">
            <div class="panel-heading">{{ucwords($type)}}</div>
            <div class="panel-body">
                @foreach($settings->where('type', $type) as $setting)
                    @include('Back.includes.settingsInput', ['setting' => $setting])
                    @if($setting->key == 'about_app_ar')
                        <script>
                            CKEDITOR.replace('about_app_ar', { height: '400px', extraPlugins: 'forms' });
                            CKEDITOR.instances.about_app_ar.setData(`{!! isset($setting) ? $setting->value : '' !!}`);
                        </script>
                    @elseif($setting->key == 'about_app_en')
                        <script>
                            CKEDITOR.replace('about_app_en', { height: '400px', extraPlugins: 'forms' });
                            CKEDITOR.instances.about_app_en.setData(`{!! isset($setting) ? $setting->value : '' !!}`);
                        </script>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@empty
    <div class="alert alert-info">@lang('back.no-var', ['var' => trans('back.settings')])</div>
@endforelse
