<x-model-index-page model="role" modelType="female">
    <table class="table table-delete-action-now" id="roles" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
    <x-slot name="scripts">
        <script>
            $(document).ready( function () {
                let renderActionColumn = function (data, type, item)
                {
                    let rootPath = '{{request()->root().'/'.app()->getLocale()}}';
                    let editTrans = '@lang('back.edit')';
                    let delTrans = '@lang('back.delete')';
                    let flooting = "{{ floating('right', 'left') }}";

                    return `
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

                                <ul class="dropdown-menu dropdown-menu-${flooting}">
                                    <li><a href="${rootPath+'/admin-panel/roles/'+item.id+'/edit'}"><i class="icon-database-edit2"></i>${editTrans}</a></li>
                                    <li>
                                        <a data-id="${item.id}" href="javascript:void(0);" class="delete-action">
                                            <i class="icon-database-remove"></i>${delTrans}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    `;
                };

                let languages = {
                    "paginate": {
                        "previous": "@lang('pagination.previous')",
                        "next": "@lang('pagination.next')",
                        "first": "@lang('datatables.first')",
                        "last": "@lang('datatables.last')",
                    },
                    "search": "@lang('datatables.search') : ",
                    "infoEmpty": "@lang('datatables.emptyshowing')",
                    "info": "@lang('datatables.showResult')",
                    "emptyTable": "@lang('datatables.no-data-available')",
                    "infoFiltered": "@lang('datatables.infoFiltered')",
                    "zeroRecords": "@lang('datatables.zeroRecords')",
                    "loadingRecords": "&nbsp;",
                    "processing"	: "<i class='fa fa-3x fa-asterisk fa-spin'></i>"
                };

                let ajaxRequest = { url : "{{ route('roles.apiIndex') }}", dataSrc : "data"};

                $('table#roles').DataTable({
                    "language": languages,
                    processing : true,
                    serverSide : true,
                    autoWidth: false,
                    columnDefs: [
                        {
                            orderable: true,
                            width: '100px',
                            targets: [5]
                        }
                    ],
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    ajax : ajaxRequest,
                    columns : [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'status', render: renderStatus},
                        {data: 'since', name: 'since'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: "id", render : renderActionColumn},
                    ],
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function() {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    },
                });

                $('.dataTables_filter input[type=search]').attr('placeholder','@lang('datatables.type-to-filter')');

                $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity, width: 'auto' });
            });

            $(".table-delete-action-now").on("click", ".delete-action", function() {

                let clickedBtn = $(this);
                let datatable = clickedBtn.parents('table').dataTable();
                let id = clickedBtn.data('id');
                let deleteMessage = '{{ trans('back.delete-message-var', ['var' => trans('back.t-role'), 'type' => trans('back.for-female')]) }}';
                let deleteMessageTitle = '{{ trans('back.delete-message-title') }}';
                let ajaxUrl = '{{ route('roles.ajax-delete-role') }}';

                swal({title: deleteMessage, text: deleteMessageTitle, icon: "warning", buttons: true, dangerMode: true}).then(willDelete => {
                    if (willDelete) {
                        $.ajax({
                            type: 'POST',
                            url: ajaxUrl,
                            data: { id },
                            success: function(response)
                            {
                                if (response.deleteStatus)
                                {
                                    clickedBtn.parents('tr').fadeOut('slow', () => datatable.fnDeleteRow($(this)));
                                    swal(response.message, {icon: "success"});
                                }
                                else
                                {
                                    swal(response.error);
                                }
                            },
                            error: (x) => crud_handle_server_errors(x)
                        });
                    } else {
                        swal({title: "@lang('back.a-message')", text: "@lang('back.operation-terminated')", icon: "success", button: "@lang('back.ok')"});
                    }
                });
            });
        </script>
    </x-slot>
</x-model-index-page>
