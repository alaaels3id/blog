@include('Back.includes.translate', ['object' => 'roles', 'fields' => [
	['name' => 'name', 'type' => 'text', 'trans' => 'back.form-name'],
]])

@include('Back.includes.permissions', ['role' => isset($currentModel) ? $currentModel : null])

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
