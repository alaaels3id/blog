<div id="view_message_details" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" dir="{{direction()}}">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1 class="modal-title">@lang('back.message-details')</h1>
            </div>

            <div class="modal-body">
                <h6 class="text-semibold">@lang('back.from') : {{$message->email}}</h6>
                <p>
                    @lang('back.t-contact') : {{$message->message}}
                </p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
