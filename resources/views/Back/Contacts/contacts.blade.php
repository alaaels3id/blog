@extends('Back.layouts.master')

@section('title', trans('back.contacts'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title"><x-page-title :title="trans('back.contacts')"></x-page-title></div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li class="active">@lang('back.contacts')</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        @include('includes.flash')
        <div class="panel-heading">
            <h5 class="panel-title" dir="{{ direction() }}" style="float: {{ floating('right', 'left') }};">
                @lang('back.contacts') ({{ $contacts->count() }})
            </h5>
            <ul class="breadcrumb-elements" dir="{{ direction() }}" style="float: {{ floating('left', 'right') }};">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear position-left"></i></a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('contacts.export') }}"><i class="icon-file-excel"></i> @lang('back.export-csv')</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <table class="table datatable-basic" id="contacts" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>@lang('back.form-message')</th>
                <th>@lang('back.since')</th>
                <th>@lang('back.updated_at')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($contacts as $key => $contact)
                <tr id="contact-row-{{ $contact->id }}">
                    <td>{{ $key+1 }}</td>

                    <td>
                        <a
                            href='{{route('contacts.message.details', ['id' => $contact->id])}}'
                            class='btn btn-md btn-default message-details-btn'
                            type='button'
                            data-toggle='tooltip'>{{ str()->limit($contact->name ?? trans('back.no-value'),30) }}
                        </a>
                    </td>

                    <td>{{ str()->limit($contact->message, 30) }}</td>

                    <td>{{ $contact->created_at->diffForHumans() }}</td>

                    <td>{{ $contact->updated_at->diffForHumans() }}</td>

                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

                                <ul class="dropdown-menu dropdown-menu-{{ floating('right', 'left') }}">
                                    <li>
                                        <a href="{{route('contacts.send-user-message', $contact->id)}}" class="send-user-message-btn">
                                            <i class="icon-bubbles2"></i>@lang('back.replay')
                                        </a>
                                        <a data-id="{{$contact->id}}" class="delete-action" href="{{localeUrl('/admin-panel/contacts/'.$contact->id)}}">
                                            <i class="icon-database-remove"></i>@lang('back.delete')
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
    @include('Back.includes.deleteActionScript', ['model' => 'contact', 'modelType' => 'female'])
    <script>
        let contacts_table = $('table#contacts');

        contacts_table.on('click', 'a.message-details-btn', function () {
            let clickedMessageAnchor = $(this);
            let ajaxUrl = clickedMessageAnchor.attr('href');
            let siteModel = $('div#site-modals');
            $.ajax({
                type: 'GET',
                url: ajaxUrl,
                success: function (response) {
                    siteModel.html(response);
                    if (response.requestStatus && response.requestStatus === false) {
                        siteModel.html('');
                    } else {
                        $('#view_message_details').modal('show');
                    }
                },
                error: (x) => crud_handle_server_errors(x)
            });
            return false;
        });

        contacts_table.on('click', 'a.send-user-message-btn', function () {
            let clickedMessageAnchor = $(this);
            let ajaxUrl = clickedMessageAnchor.attr('href');
            let siteModel = $('div#site-modals');
            $.ajax({
                type: 'GET',
                url: ajaxUrl,
                success: function (response) {
                    siteModel.html(response);
                    if (response.requestStatus && response.requestStatus === false) {
                        siteModel.html('');
                    } else {
                        $('#view_send_message').modal('show');
                    }
                },
                error: (x) => crud_handle_server_errors(x)
            });
            return false;
        });
    </script>
@stop
