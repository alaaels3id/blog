<?php

return [
    'footer' => 'All Rights reserved to the company',
    'all-rights-reserved' => 'All Rights Reserved',
    'dashboard' => 'Dashboard',
    'support' => 'Support',
    'main' => 'Main',
    'send-mail' => 'Send Mail Message',
    'ar' => 'Arabic',
    'en' => 'English',
    'print' => 'Print',
    'to' => 'To',
    'subject' => 'Subject',
    'submit' => 'Submit',
    'message' => 'Enter your message here',
    'profile' => 'Profile',
    'profile-info' => 'Profile information',
    'account-settings' => 'Account settings',
    'save' => 'Save',
    'no-value' => 'No Value',
    'type' => 'Type',
    'super-admin' => 'Super admin',
    'no-var' => 'No :var',
    'about-site' => 'About website',
    'table-truncate' => 'Table in now empty',
    'side-services' => 'Side service after booking the hall',
    'send' => 'Send',
    'our-services' => 'Our Services',
    'user-data' => 'Registeration',
    'added-by' => 'Added By',
    'confirm-reservation' => 'Confirm Reservation',
    'personal-info' => 'Personal Information',
    'personal-info-edit' => 'Edit Personal Information',
    'new' => 'New',
    'admin-login-card' => 'Enter your credentials below',
    'edit-done' => ':var Updated Successfully',
    'add-done' => ':var Added Successfully',
    'delete-done' => ':var Deleted Successfully',
    'restore-done' => ':var Restored Successfully',
    'remove-done' => ':var Removed Successfully',
    'my-account' => 'My account',
    'logout' => 'Logout',
    'login' => 'Login',
    'provider-name' => 'Provider',
    'capacity' => 'Capacity',
    'about' => 'About Us',
    'terms' => 'Terms & Conditions',
    'register' => 'Register',
    'quick-links' => 'Quick Links',
    'user-name' => 'User Name',
    'product-name' => 'Product Name',
    'display-all' => 'Show All :var',
    'var-report' => ':var Reports',
    'reports' => 'Reports',
    'page-number' => 'Page :var',
    'contact-numbers' => 'Contact Numbers',
    'contact-us' => 'Contact Us',
    'about-us' => 'About Us',
    'contact-us-title' => 'We have a dedicated team to respond to your inquiry',
    'write-your-message' => 'Write your message ...',
    'tenders' => 'Tenders',
    'notifications' => 'Notifications',
    'not-started' => 'Not Started',
    'started' => 'Started',
    'still' => 'Ongoing',
    'finished' => 'Finished',
    'latest-var' => 'Latest :var',
    'morning' => 'Morning, ',
    'calls' => 'Calls',
    'calls-count' => 'Calls count',
    'from' => 'From',
    'message-details' => 'Message Details',
    'replay' => 'Replay',
    'yes' => 'Yes',
    'not' => 'No',
    'is-main' => 'Show in home',
    'banners-home' => 'Premium services',
    'banners-home-title' => 'We offer you a distinguished package of services provided by the establishments',
    'projects-home' => 'Advertising and facilities services',
    'projects-home-title' => 'Some projects and services provided by major establishments',
    'project-details' => 'Details',
    'buy-project' => 'Buy Project',
    'services-home-title' => 'Agencies services',
    'agencies-login' => 'Agencies Login',
    'users-login' => 'Users Login',
    'forget-password' => 'Forget Password ?',
    'doNotHaveAccount' => 'Do not have account ?',
    'create-new-account' => 'Create new account',
    'create-new-account-for-users' => 'Create new account for persons',
    'create-new-account-for-agencies' => 'Create new account for agencies',
    'reset-password' => 'Restore password',
    'reset-new-password' => 'Password Reset',
    'reset-password-title' => 'Please enter your email to recover the password',
    'set-new-password-title' => 'Please enter the new password',
    'change-current-password' => 'Change current password',
    'current-password' => 'Current password',
    'new-password' => 'New Password',
    'activation-code' => 'Activation Code',
    'activation-code-title' => 'Please enter the activation code sent to your email',
    'subscriptions' => 'Subscriptions',
    'my-services' => 'My Services',
    'my-projects' => 'My Projects',
    'my-tenders' => 'My Tenders',
    'subscription-features' => 'Subscription features',
    'order-service' => 'Order Service',
    'service-details' => 'Service Details',
    'order-service-form' => 'Order Service Form',
    'order-service-name' => 'Service Name',
    'order-service-type' => 'Type of activity',
    'order-service-desc' => 'Description of the required service',
    'order-service-extra' => 'Service-specific accessories',
    'order-service-message' => 'Service order sent successfully',
    'please-enter-this-data' => 'Please fill the following fields',
    'buy-project-message' => 'Request sent successfully',
    'subscriptions-title' => 'Quickly subscribe to enjoy more services',
    'package-details' => 'Package Details',
    'subscribe' => 'Subscribe',
    'about-agency' => 'About Agency',
    'similar-agency' => 'Similar agencies',
    'pay' => 'Payment',
    'account-has-been-disactive'=>'Your account has been deactivated by the administrator',
    'invalid-account-info'=>'Your credentials are incorrect',
    'login-form-header'=>'Login to your account',
    'add-new-stepTypes'=>'Add Work units types',
    'add-new-steps'=>'Add Project Steps',
    'add-new-project-tools'=>'Add Project Tools',
    'add-new-project'=>'Add New Project',
    'add-logs'=>'Add Logs',
    'add-new-changeLog'=>'Add New Changes Log',
    'add-new-accreditationLogs'=>'Add Accreditations Log',
    'add-new-auditLog'=>'Add Audit Log',

    'home' => 'Home',
    'users' => 'Users',
    'ways' => 'Ways',
    'markets' => 'Markets',
    'delegates' => 'Delegates',
    'admins' => 'Admins',
    'products' => 'Products',
    'categories' => 'Categories',
    'cities' => 'Cities',
    'settings' => 'Settings',
    'services' => 'Services',
    'sliders' => 'Sliders',
    'classifications' => 'Classification',
    'coupons' => 'Coupons',
    'contacts' => 'Messages',
    'payments' => 'Payed Coupons',
    'commissions' => 'Commissions',
    'roles' => 'Roles',
    'permissions' => 'Roles',
    'providers' => 'Providers',
    'posts' => 'Posts',
    'videos' => 'Videos',
    'pharmacies' => 'Pharmacies',
    'banners' => 'Banners',
    'faqs' => 'F&Q',
    'suggestions' => 'Suggestions',
    'covids' => 'Covids',
    'owners' => 'Owners',
    'orders' => 'Orders',
    'agencies' => 'Agencies',
    'agencies-guide' => 'Agencies Guide',
    'already-have-account' => 'Already have account ? ',
    'projects' => 'Projects',
    'achievements' => 'Agencies Achievements',
    'pages' => 'Pages',
    'sections' => 'Sections',
    'packages' => 'Packages',
    'banks' => 'Banks',
    'bankTransfers' => 'Bank Transfers',
    'albums' => 'Albums',
    'photos' => 'Photos',
    'countries' => 'Countries',
    'steps' => 'Steps',
    'stepTypes' => 'Step Types',
    'tools' => 'Project Tools',
    'changesLogs' => 'Changes Logs',
    'accreditationLogs' => 'Accreditations Logs',
    'auditLogs' => 'Audit Logs',
    'userRoles' => 'User\'s Roles',
    'logs' => 'Logs',
    'surveys' => 'Surveys',
    'questions' => 'Questions',
    'answers' => 'Answers',
    'galleries' => 'Galleries',
    'championships' => 'Championships',
    'courses' => 'Courses',
    'complaints' => 'Complaints',

    'n-users' => 'Users',
    'n-ways' => 'Ways',
    'n-markets' => 'Markets',
    'n-delegates' => 'Delegates',
    'n-admins' => 'Admins',
    'n-products' => 'Products',
    'n-categories' => 'Categories',
    'n-cities' => 'Cities',
    'n-settings' => 'Settings',
    'n-services' => 'Services',
    'n-sliders' => 'Sliders',
    'n-classifications' => 'Classification',
    'n-coupons' => 'Coupons',
    'n-contacts' => 'Messages',
    'n-payments' => 'Payed Coupons',
    'n-commissions' => 'Commissions',
    'n-roles' => 'Roles',
    'n-permissions' => 'Roles',
    'n-providers' => 'Providers',
    'n-posts' => 'Posts',
    'n-videos' => 'Videos',
    'n-pharmacies' => 'Pharmacies',
    'n-banners' => 'Banners',
    'n-faqs' => 'F&Q',
    'n-suggestions' => 'Suggestions',
    'n-covids' => 'Covids',
    'n-owners' => 'Owners',
    'n-orders' => 'Orders',
    'n-agencies' => 'Agencies',
    'n-projects' => 'Projects',
    'n-achievements' => 'Agencies Achievements',
    'n-pages' => 'Pages',
    'n-sections' => 'Sections',
    'n-tenders' => 'Tenders',
    'n-packages' => 'Packages',
    'n-banks' => 'Banks',
    'n-bankTransfers' => 'Bank Transfers',
    'n-albums' => 'Albums',
    'n-photos' => 'Photos',
    'n-countries' => 'Countries',
    'n-steps' => 'Steps',
    'n-stepTypes' => 'Step Types',
    'n-tools' => 'Project Tools',
    'n-changesLogs' => 'Changes Logs',
    'n-accreditationLogs' => 'Accreditations Logs',
    'n-auditLog' => 'Audit Logs',
    'n-userRoles' => 'User\'s Roles',
    'n-surveys' => 'Surveys',
    'n-questions' => 'Questions',
    'n-answers' => 'Answers',
    'n-galleries' => 'Galleries',
    'n-championships' => 'Championships',
    'n-courses' => 'Courses',
    'n-complaints' => 'Complaints',

    't-user' => 'The User',
    't-way' => 'The Way',
    't-delegate' => 'The Delegate',
    't-market' => 'The Market',
    't-admin' => 'The Admin',
    't-product' => 'The Product',
    't-category' => 'The Category',
    't-city' => 'The City',
    't-service' => 'The Service',
    't-setting' => 'The Setting',
    't-classification' => 'The Classification',
    't-coupon' => 'The Coupon',
    't-contact' => 'The Message',
    't-role' => 'The Role',
    't-permission' => 'The Permission',
    't-provider' => 'The Provider',
    't-post' => 'The Post',
    't-video' => 'The Video',
    't-pharmacy' => 'The Pharmacy',
    't-banner' => 'The Banner',
    't-faq' => 'The Faq',
    't-suggestion' => 'The Suggestion',
    't-covid' => 'The Covid',
    't-owner' => 'The Owner',
    't-order' => 'The Order',
    't-agency' => 'The Agency',
    't-project' => 'The Project',
    't-tender' => 'The Tender',
    't-achievement' => 'The Agency Achievement',
    't-page' => 'The Page',
    't-section' => 'The Section',
    't-package' => 'The Package',
    't-bank' => 'The Bank',
    't-bankTransfer' => 'The Bank Transfer',
    't-album' => 'The Album',
    't-photo' => 'The Photo',
    't-country' => 'The Country',
    't-step' => 'The Step',
    't-stepType' => 'The Step Type',
    't-tool' => 'The Project Tool',
    't-changesLog' => 'The Change Log',
    't-accreditationLog' => 'The Accreditation Log',
    't-auditLog' => 'The Audit Log',
    't-userRole' => 'The User\'s Role',
    't-log' => 'The Log',
    't-survey' => 'The Survey',
    't-question' => 'The Question',
    't-answer' => 'The Answer',
    't-gallery' => 'The Gallery',
    't-championship' => 'The Championship',
    't-course' => 'The Course',
    't-complaint' => 'The Complaint',

    'user' => 'User',
    'delegate' => 'Delegate',
    'way' => 'Way',
    'market' => 'Market',
    'admin' => 'Admin',
    'product' => 'Product',
    'category' => 'Category',
    'city' => 'City',
    'service' => 'Service',
    'setting' => 'Setting',
    'classification' => 'Classification',
    'coupon' => 'Coupon',
    'contact' => 'Message',
    'role' => 'Role',
    'permission' => 'Permission',
    'provider' => 'Provider',
    'post' => 'Post',
    'video' => 'Video',
    'pharmacy' => 'Pharmacy',
    'banner' => 'Banner',
    'faq' => 'Faq',
    'suggestion' => 'Suggestion',
    'covid' => 'Covid',
    'owner' => 'Owner',
    'order' => 'Order',
    'agency' => 'Agency',
    'project' => 'Project',
    'tender' => 'Tender',
    'achievement' => 'Agency Achievement',
    'page' => 'Page',
    'album' => 'Album',
    'section' => 'Section',
    'package' => 'Package',
    'bank' => 'Bank',
    'bankTransfer' => 'Bank Transfer',
    'photo' => 'Photo',
    'country' => 'Country',
    'step' => 'Step',
    'stepType' => 'Step Type',
    'tool' => 'Project Tool',
    'changeLog' => 'Change Log',
    'accreditationLog' => 'Accreditation Log',
    'auditLog' => 'Audit Log',
    'userRole' => 'User\'s Role',
    'log' => 'Log',
    'survey' => 'Survey',
    'question' => 'Question',
    'answer' => 'Answer',
    'gallery' => 'Gallery',
    'championship' => 'Championship',
    'course' => 'Course',
    'complaint' => 'Complaint',

    'total' => 'Total',
    'slider' => 'Slider',
    'edit-profile' => 'Edit Profile',
    'privacy' => 'Privacy',
    'close' => 'Close',
    'count-var' => ':var count',
    'agree' => 'Agree',
    'disagree' => 'Disagree',
    'conditions' => 'Terms & Conditions',
    'disactive-delegates' => 'Disactive Delegates',
    'active-delegates' => 'Active Delegates',
    'disactive-markets' => 'Disactive Delegates',
    'active-markets' => 'Active Delegates',
    'disactive-coupons' => 'Disactive Coupons',
    'active-coupons' => 'Active Coupons',
    'updated_at' => 'Last update',
    'deleted_at' => 'Deleted at',
    'location' => 'Location',
    'send-notification' => 'Send Notification',
    'pending' => 'Pending',
    'accepted' => 'Accepted',
    'accept' => 'Accept',
    'refuse' => 'Refuse',
    'refused' => 'Refused',
    'register-agency-message' => 'Registration completed successfully, pending management approval',
    'subscription-still-active' => 'Sorry your subscription is still active',
    'wait-for-admin-response' => 'Sorry, please wait for the administration\'s response to your last bank transfer',
    'create-var' => 'Create new :var',
    'edit-var' => 'Edit :var',
    'add' => 'Add',
    'all' => 'Show All',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'since' => 'Since',
    'active' => 'Active',
    'disactive' => 'Disactive',
    'export-csv' => 'Export to .csv',
    'reyal' => 'SR',
    'confirm' => 'Confirm',
    'next' => 'Next',
    'no' => 'No :var',
    'more' => 'More',
    'max-quantity' => 'Sorry, you entered a bigger quantity that stored with us !',
    'no-enough-quantity' => 'No Enough Quantity',
    'trashed' => 'Trashed',
    'restore' => 'Restore',
    'file' => 'File',
    't-file' => 'The File',
    'map' => 'Map',
    'text' => 'Input Text',
    'textarea' => 'Input Textarea',
    'ckeditor' => 'Input CKeditor',
    'image' => 'Input Image File',
    'select-a-value' => 'Select a value',
    'ended-at-date-after' => 'Sorry, the booking date must be later than today\'s date',
    'is-used' => 'Used Case',
    'used' => 'Used',
    'not-used' => 'Not Used',
    'phone-digits-between' => 'The phone must be 9 numbers.',
    'send-a-message' => 'Send new Mail',
    'pharmacy-owner' => 'Owner',
    'available' => 'Available',
    'unavailable' => 'Unavailable',
    'pharmacy_owner' => 'Pharmacy Owner',
    'post-creator' => 'Publisher',
    'active-transfer' => 'Active Transfer',
    'covid-new-cases' => 'New Cases',
    'covid-cases-from-pos-to-nig' => 'Cases from positive to negative',
    'covid-new-deaths' => 'New Deaths',
    'covid-total-cases' => 'Total Cases',
    'covid-recovery-cases' => 'recovery-cases',
    'covid-total-deaths' => 'Total Deaths',
    'var-in-lang' => ':var in :lang',
    'important-achievements' => 'Important achievements',
    'bio' => 'Bio',
    'permissions-error-page' => 'Sorry, you do not have the permission to see this',
    'days' =>
        [
            'day-var' => 'This :var',
            'saturday' => 'Saturday',
            'sunday' => 'Sunday',
            'monday' => 'Monday',
            'tuesday' => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday' => 'Thursday',
            'friday' => 'Friday',
        ],
    'online' => 'Online',

    'form-name' => 'Name',
    'form-email' => 'Email',
    'form-key' => 'Key',
    'form-value' => 'Value',
    'form-select' => 'Select Type',
    'form-phone' => 'Mobile Phone',
    'form-city' => 'City',
    'form-address' => 'Address',
    'form-image' => 'Image',
    'form-additional-image' => 'Additional Image',
    'form-ammount' => 'Ammount',
    'form-price' => 'Price',
    'form-state' => 'State',
    'form-street' => 'Street',
    'form-bride-name' => 'Bride name',
    'form-password' => 'Password',
    'form-actions' => 'Actions',
    'form-password-confirm' => 'Password Confirmation',
    'form-video-link' => 'Video Link',
    'form-cost' => 'Booking Cost',
    'form-number' => 'Number',
    'form-status' => 'Status',
    'form-latitude' => 'Latitude',
    'form-longitude' => 'Longitude',
    'form-category' => 'Category',
    'form-description' => 'Description',
    'form-start-time' => 'First Period Start Time',
    'form-end-time' => 'First Period End Time',
    'form-identity' => 'Identity .no',
    'form-bank-account-no' => 'Bank Account .no',
    'form-bank-name' => 'Bank Name',
    'form-how-he-know' => 'Way',
    'form-shop-field' => 'Shop Field',
    'form-shop-name' => 'Shop Name',
    'form-ended-at' => 'Ended at',
    'form-second-start-time' => 'Second Period Start Time',
    'form-second-end-time' => 'Second Period end Time',
    'form-message' => 'Message',
    'form-whatsapp-phone' => 'Whatsapp Number',
    'form-expired-date' => 'Expiration date',
    'form-year-cost' => 'Year Cost',
    'form-years' => 'Subscription years',
    'form-coupon-value' => 'Coupon value',
    'form-subscription-no' => 'Subscription .No',
    'form-date-from' => 'Date From',
    'form-date-to' => 'Date To',
    'form-payed-at' => 'Payed At',
    'form-amount' => 'Total',
    'form-currency' => 'Currency',
    'form-transaction-id' => 'Transaction ID',
    'form-order-id' => 'Order ID',
    'form-input' => 'Input Type',
    'form-type' => 'Tab Type',
    'form-database' => 'Database',
    'form-body' => 'Content',
    'form-title' => 'Title',
    'form-types' => 'Type',
    'form-has-delivery' => 'Has Delivery',
    'form-availability' => 'Work availability',
    'form-question' => 'Question',
    'form-answer' => 'Answer',
    'form-year-founded' => 'Year Founded',
    'form-crn' => 'Commercial Registration No',
    'form-specialization' => 'Specialization',
    'form-start-date' => 'Start Date',
    'form-end-date' => 'End Date',
    'form-slug' => 'Page Slug',
    'form-page-content' => 'Page Content',
    'form-tender-name' => 'Tender Name',
    'form-tender-desc' => 'Tender Description',
    'form-agency-logo' => 'Agency Logo',
    'form-apply-now' => 'Apply Now',
    'form-apply-done' => 'Applying Done',
    'form-package-days' => 'Package Days',
    'form-your-price' => 'Your Price',
    'form-account-number' => 'Account Number',
    'form-iban-number' => 'iban Number',
    'form-transfer-name' => 'Transfer\'s owner name',
    'form-buy-date' => 'Date of buy',
    'form-whatsapp' => 'Whatsapp',
    'form-job' => 'Job',
    'form-age' => 'Age',
    'form-unit-price' => 'Unit Price',
    'form-qualification' => 'Qualification',
    'form-experience' => 'Experience',
    'form-previous-work' => 'Previous Work',
    'form-previous-projects' => 'Previous Projects',
    'form-previous-companies' => 'Previous Companies',
    'form-friend-name' => 'Friend Name',
    'form-friend-phone' => 'Friend Phone',
    'form-country-code' => 'Country Key',
    'form-cv' => 'C.V',
    'form-file' => 'File',
    'form-project-number' => 'Project Number',
    'form-project-program' => 'Project Program',
    'form-approved-budget' => 'Approved Budget',
    'form-internal-range' => 'Project scope of work',
    'form-external-range' => 'Outside the scope of the project',
    'form-entity' => 'Entity',
    'form-assumptions' => 'Assumptions',
    'form-completion-rate' => 'Completion rate',
    'form-project-budget-disbursement-plan' => 'Project budget disbursement plan',
    'form-accreditation' => 'Accreditation',
    'form-initiative-duration' => 'Initiative Duration',
    'form-importance' => 'Importance',
    'form-effect'=>'Effect',
    'form-administrator'=>'Administrator',
    'form-ability-to-influence' => 'Ability to influence',
    'form-impact-level' => 'Impact level',
    'form-linkedin' => 'Linkedin Account',
    'form-role' => 'Role',
    'form-initial-action' => 'Initial action',
    'form-expected-duration' => 'Expected Duration',
    'form-expected-start-date' => 'Expected Start Date',
    'form-expected-end-date' => 'Expected End Date',
    'form-actual-start-date' => 'Actual Start Date',
    'form-actual-end-date' => 'Actual End Date',
    'form-steps-dependent-on-it' => 'Steps Dependent On It',
    'form-define-the-project-team' => 'Project Team Definition',
    'form-team-permissions' => 'Permissions',
    'form-team-unit-count' => 'Work unit number',
    'form-date' => 'Date',
    'form-release' => 'Release',
    'form-notes' => 'Notes',
    'form-link' => 'Link',
    'form-agency' => 'Agency',
    'form-belt' => 'Belt Degree',
    'form-weight' => 'Weight',

    'email-format-checker' => 'The Email address format is invalid.',
    'country-key-checker' => 'The Code format is invalid.',
    'delete-message-var' => 'Are you sure you want to delete :type :var',
    'for-male' => 'this',
    'for-female' => 'this',
    'male'=>'male',
    'female'=>'female',
    'delete-message-title' => 'It will be deleted to the Recycle Bin',
    'a-message' => 'Message',
    'a-process-canceled' => 'Operation terminated',
    'month' => 'Month',
    't-month' => 'The Month',
    'months' =>
        [
            'jan' => 'January',
            'feb' => 'February',
            'mar' => 'March',
            'apr' => 'April',
            'may' => 'May',
            'jun' => 'June',
            'jul' => 'July',
            'aug' => 'August',
            'sep' => 'September',
            'oct' => 'October',
            'nov' => 'November',
            'dec' => 'December',
        ],
    'operation-terminated' => 'The operation has been canceled',
    'ok' => 'O.K',
    'change-password-success' => 'Password Changed Successfully',
    'old-password-incorrect' => 'Sorry the password is incorrect',
    'data-edited-successfully' => 'The data has been modified successfully',
    'log-viewer'=>'Log Viewer',
    'public'=>'Public',
    'private'=>'Private',
    'representative'=>'Representative',
    'member'=>'Team Member',
    'project-manger'=>'Project Manger',
    'project-leader'=>'Project Leader',
    'program-manger'=>'Program Leader',
    'gender'=>'Gender',
    'kilo'=>'KG',
    'concerns-list'=>'Concerns list',
    'risks' => 'Risks',
    'restrictions' => 'Limitations and limitations',
    'project-structure'=>'Project organization and structure',
    'team'=>'Project Teamwork',
    'project-report'=>'Project performance report',
    'latest-users'=>'Latest Users',
    'users-performance-report'=>'Users Performance report',
    'user-performance-report'=>'User Performance report',
    'choose-file'=>'Choose File',
    'no-file-selected'=>'No File Selected',
    'text-must-be-arabic'=>'Field must be in arabic language',
    'text-must-be-english'=>'Field must be in english language',
    'total-unit-count'=>'Total units',

    'survey-radio-button' => 'Radio Button',
    'survey-checkbox' => 'Checkbox',
    'survey-dropdown' => 'Dropdown',
    'survey-imageType' => 'Image Type',
    'survey-rating-scale' => 'Rating Scale',
    'survey-star-rating' => 'Star Rating',
    'survey-nps' => 'NPS',
    'survey-slider-scale' => 'Slider Scale',
    'survey-ranking' => 'Ranking',
    'survey-matrix-radio' => 'Matrix Radio',
    'survey-matrix-checkbox' => 'Matrix Checkbox',
    'tasks'=>'Tasks',
    'federation-activities'=>'Federation activities',
    'international-agenda'=>'International agenda',
    'gallery-type'=>'Gallery Type',
    'trainings'=>'Trainings',
    'kata'=>'Kata',
    'kumite'=>'Kumite',
    'championship-type'=>'Championship Type',
    'cubs_and_girls'=>'Cubs and Girls',
    'budding'=>'Male and female youth',
    'youth'=>'Youth and Women',
    'men_and_women'=>'Men and women',
    'banner-create-image'=>'The image field is required'
];
