<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFcmsTable extends Migration
{
    public function up()
    {
        Schema::create('fcms', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->text('fcm')->nullable();
            $table->nullableMorphs('fcmable');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('fcms');
    }
}
