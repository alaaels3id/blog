<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();

            $table->unsignedBigInteger('chat_id')->nullable();
            $table->foreign('chat_id')->references('id')->on('chats')->onDelete('cascade');

            $table->unsignedBigInteger('from_user')->nullable();
            $table->foreign('from_user')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('to_user')->nullable();
            $table->foreign('to_user')->references('id')->on('users')->onDelete('cascade');

            $table->string('message')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
