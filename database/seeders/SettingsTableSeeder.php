<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->insert([
            self::setRow('map_api', 'Map', 'API', 0),
            self::setRow('notification_key', 'Notification Key', 'API', 0),

            self::setRow('smtp', 'driver', 'SMTP', 0, 'smtp'),
            self::setRow('smtp_port', 'smtp port', 'SMTP', 0, '2525'),
            self::setRow('smtp_host', 'smtp host', 'SMTP', 0, 'smtp.mailtrap.io'),
            self::setRow('smtp_sender_email', 'smtp sender name', 'SMTP', 0, env('APP_NAME')),
            self::setRow('smtp_sender_name', 'smtp sender email', 'SMTP', 0, 'info@site.com'),
            self::setRow('smtp_encryption', 'smtp encryption', 'SMTP', 0),
            self::setRow('smtp_username', 'smtp username', 'SMTP', 0, 'efc9853ca1a258'),
            self::setRow('smtp_password', 'smtp password', 'SMTP', 0, '55b6b6f0114f15'),

            self::setRow('about_app_ar', 'about app ar', 'APP', 4, ''),
            self::setRow('about_app_en', 'about app en', 'APP', 4, ''),

            self::setRow('app_terms_ar', 'app terms ar', 'APP', 4, ''),
            self::setRow('app_terms_en', 'app terms en', 'APP', 4, ''),
            self::setRow('app_version', 'app version', 'APP', 0, '1.0.0'),

            self::setRow('contact_facebook', 'صفحة الفيسبوك', 'CONTACTS', 0, ''),
            self::setRow('contact_twitter', 'صفحة تويتر', 'CONTACTS', 0, ''),
            self::setRow('contact_instgram', 'صفحة انستجرام', 'CONTACTS', 0, ''),
            self::setRow('contact_linkedin', 'صفحة linkedin', 'CONTACTS', 0, ''),
            self::setRow('contact_phone', 'رقم الهاتف', 'CONTACTS', 0, ''),
            self::setRow('contact_email', 'البريد الالكتروني', 'CONTACTS', 0, ''),
        ]);
    }

    private static function setRow($key, $name, $type, $input, $value = null)
    {
        return [
            'key'        => $key,
            'name'       => $name,
            'type'       => $type,
            'input'      => $input,
            'value'      => $value,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
