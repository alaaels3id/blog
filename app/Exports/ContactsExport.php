<?php

namespace App\Exports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\FromCollection;

class ContactsExport implements FromCollection
{
    public function collection()
    {
        return Contact::select(['id','name','message','created_at'])->get();
    }
}
