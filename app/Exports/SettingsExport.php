<?php

namespace App\Exports;

use App\Models\Setting;
use Maatwebsite\Excel\Concerns\FromCollection;

class SettingsExport implements FromCollection
{
    public function collection()
    {
        return Setting::select(['id','key','value','created_at'])->get();
    }
}
