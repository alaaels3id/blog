<?php

namespace App\Exports;

use App\Models\Championship;
use Maatwebsite\Excel\Concerns\FromCollection;

class ChampionshipsExport implements FromCollection
{
    public function collection()
    {
        return Championship::select(['id', 'created_at'])->get();
    }
}
