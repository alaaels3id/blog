<?php

namespace App\Exports;

use App\Models\Admin;
use Maatwebsite\Excel\Concerns\FromCollection;

class AdminsExport implements FromCollection
{
    public function collection()
    {
        return Admin::where('role_id', '!=', 1)->select(['name', 'email', 'created_at'])->get();
    }
}
