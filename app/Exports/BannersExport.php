<?php

namespace App\Exports;

use App\Models\Banner;
use Maatwebsite\Excel\Concerns\FromCollection;

class BannersExport implements FromCollection
{
    public function collection()
    {
        return Banner::select(['id', 'created_at'])->get();
    }
}
