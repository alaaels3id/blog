<?php

namespace App\Exports;

use App\Models\Gallery;
use Maatwebsite\Excel\Concerns\FromCollection;

class GalleriesExport implements FromCollection
{
    public function collection()
    {
        return Gallery::select(['id', 'type', 'file_type', 'name', 'created_at'])->get();
    }
}
