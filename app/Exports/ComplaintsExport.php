<?php

namespace App\Exports;

use App\Models\Complaint;
use Maatwebsite\Excel\Concerns\FromCollection;

class ComplaintsExport implements FromCollection
{
    public function collection()
    {
        return Complaint::select(['id', 'name', 'belt', 'category', 'agency', 'complaint', 'created_at'])->get();
    }
}
