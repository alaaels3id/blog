<?php

namespace App\Exports;

use App\Models\Course;
use Maatwebsite\Excel\Concerns\FromCollection;

class CoursesExport implements FromCollection
{
    public function collection()
    {
        return Course::select(['id', 'created_at'])->get();
    }
}
