<?php

namespace App\Exports;

use App\Models\Training;
use Maatwebsite\Excel\Concerns\FromCollection;

class TrainingsExport implements FromCollection
{
    public function collection()
    {
        return Training::select(['id', 'name', 'belt', 'agency', 'created_at'])->get();
    }
}
