<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CountryCodeRule implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/^\+\d{3,4}$/', $value);
    }

    public function message()
    {
        return trans('back.country-key-checker');
    }
}
