<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsAr implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/[اأإء-ي]/ui', $value, $match);
    }

    public function message()
    {
        return trans('back.text-must-be-arabic');
    }
}
