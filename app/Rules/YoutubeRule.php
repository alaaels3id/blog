<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class YoutubeRule implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('@^(?:https://(?:www\\.)?youtube.com/)(watch\\?v=|v/)([a-zA-Z0-9_]*)@', $value, $match);
    }

    public function message()
    {
        return 'حقل رابط الفيديو يجب ان يكون رابط يوتيوب صحيح';
    }
}
