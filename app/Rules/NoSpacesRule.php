<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NoSpacesRule implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_replace('/\s+/', '', $value) === $value;
    }

    public function message()
    {
        return 'The :attribute have no spaces.';
    }
}
