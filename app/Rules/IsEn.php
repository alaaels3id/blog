<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsEn implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/', $value, $match);
    }

    public function message()
    {
        return trans('back.text-must-be-english');
    }
}
