<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailFormatChecker implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/(.*)@(gmail|outlook|yahoo)\.com/i', $value);
    }

    public function message()
    {
        return trans('back.email-format-checker');
    }
}
