<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class LowerCaseRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Str::lower($value) === $value;
    }

    public function message()
    {
        return 'The :attribute must be lowe case.';
    }
}
