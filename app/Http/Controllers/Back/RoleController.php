<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Role;
use App\Repository\Contracts\IRoleRepository;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $repo;

    public function __construct(IRoleRepository $repository)
    {
        $this->repo = $repository;
    }

    public function apiIndex()
    {
        return $this->repo->apiIndex();
    }

    public function index()
    {
        return $this->repo->index();
    }

    public function changeStatus(Request $request)
    {
        return $this->repo->changeStatus($request);
    }

    public function create()
    {
        return $this->repo->create();
    }

    public function store(Requests\Back\CreateRoleRequest $request)
    {
        return $this->repo->store($request);
    }

    public function update(Requests\Back\EditRoleRequest $request, $id)
    {
        return $this->repo->update($request, Role::find($id));
    }

    public function delete(Request $request)
    {
        return $this->repo->delete($request);
    }
}
