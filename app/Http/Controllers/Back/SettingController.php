<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\SettingsExport;
use Maatwebsite\Excel\Facades\Excel;

class SettingController extends Controller
{
    public function index()
    {
        return view('Back.Settings.index', ['settings' => Setting::all()]);
    }

    public function create()
    {
        return view('Back.Settings.create');
    }

    public function export(Request $request)
    {
        return Excel::download(new SettingsExport, 'settings.xlsx');
    }

    public function store(Requests\Back\CreateSettingRequest $request)
    {
        return Setting::createSetting($request);
    }

    public function edit(Setting $setting)
    {
        return view('Back.Settings.edit', compact('setting'));
    }

    public function update(Requests\Back\EditSettingRequest $request, $id)
    {
        return Setting::updateSetting($request, Setting::find($id));
    }

    public function UpdateAll(Request $request)
    {
        return Setting::updateAll($request);
    }
}
