<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Repository\Contracts\IUserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $repo;

    public function __construct(IUserRepository $repository)
    {
        $this->repo = $repository;
    }

    public function index()
    {
        return $this->repo->index();
    }

    public function apiIndex()
    {
        return $this->repo->apiIndex();
    }

    public function create()
    {
        return $this->repo->create();
    }

    public function edit($id)
    {
        return $this->repo->edit($id);
    }

    public function showUserMessage($id)
    {
        return $this->repo->showUserMessage($id);
    }

    public function changeStatus(Request $request)
    {
        return $this->repo->changeStatus($request);
    }

    public function forceDelete($id)
    {
        return $this->repo->forceDelete($id);
    }

    public function sendUserMessage(Request $request)
    {
        return $this->repo->showUserMessage($request);
    }

    public function sendUserNotification(Request $request)
    {
        return $this->repo->sendUserNotification($request);
    }

    public function show(User $user)
    {
        return view('Back.Users.show', compact('user'));
    }

    public function store(Requests\Back\CreateUserRequest $request)
    {
        return $this->repo->store($request);
    }

    public function update(Requests\Back\EditUserRequest $request, $id)
    {
        return $this->repo->update($request, User::find($id));
    }

    public function delete(Request $request)
    {
        return $this->repo->delete($request->id);
    }
}
