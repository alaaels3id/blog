<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\BASE_CONTROLLER;
use App\Http\Requests;
use App\Models\Admin;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Exports\AdminsExport;
use Illuminate\Support\Facades\Auth;

class AdminController extends BASE_CONTROLLER
{
    public function __construct()
    {
        parent::__construct('admin', Admin::class, new AdminsExport);
    }

    public function index()
    {
        return view('Back.Admins.index', ['admins' => Admin::where('role_id', '!=', 1)->orWhere('role_id', '=', null)->get()]);
    }

    public function showAdminMessage($id)
    {
        if (!$admin = Admin::find($id)) return response()->json(['requestStatus' => false, 'message' => trans('responseMessages.product-not-exist')]);

        return view('Back.Admins.showAdminMailSendModal', compact('admin'))->render();
    }

    public function sendAdminMessage(Request $request)
    {
        return Admin::sendAdminMessage($request);
    }

    public function showAdmin(Admin $admin)
    {
        return view('Back.Admins.show', compact('admin'));
    }

    public function home()
    {
        return view('Back.index');
    }

    public function AdminUpdateProfile(Requests\Back\EditAdminProfileRequest $request)
    {
        $adminData = $request->except(['_token', 'submit']);

        $currentAdmin = Admin::find($adminData['admin_id']);

        return Admin::updateAdmin(arr()->except($adminData, ['admin_id']), $currentAdmin);
    }

    public function adminProfile()
    {
        return view('Back.Admins.profile', ['admin' => Auth::guard('admin')->user()]);
    }

    public function create()
    {
        return view('Back.Crud.create', ['roles' => Role::getInSelectForm(), 'model' => 'admin']);
    }

    public function store(Requests\Back\CreateAdminRequest $request)
    {
        return Admin::createAdmin($request);
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', ['currentModel' => Admin::findOrFail($id), 'roles' => Role::getInSelectForm(), 'model' => 'admin']);
    }

    public function update(Requests\Back\EditAdminRequest $request, $id)
    {
        return Admin::updateAdmin($request, Admin::find($id));
    }

    public function DeleteAdmin(Request $request)
    {
        return Admin::deleteAdmin($request);
    }
}
