<?php

namespace App\Http\Controllers\Back\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Auth\AdminLoginRequest;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('adminLogout');
    }

    public function showAdminLoginForm()
    {
        return view('auth.pages.adminLogin');
    }

    public function adminLogin(AdminLoginRequest $request)
    {
        if (!Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) return back()->with('danger', trans('back.invalid-account-info'))->withInput($request->only('email'));

        if(auth()->guard('admin')->user()->status == 0)
        {
            auth()->guard('admin')->logout();

            return back()->with('warning', trans('back.account-has-been-disactive'))->withInput($request->only('email'));
        }

        return redirect()->intended(localeUrl('/admin-panel'));
    }

    public function adminLogout()
    {
        Auth::guard('admin')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();

        return redirect()->route('admin-panel');
    }
}
