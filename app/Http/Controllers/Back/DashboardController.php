<?php

namespace App\Http\Controllers\Back;

use App\Exports\TrainingsExport;
use App\Models\Training;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();

        return view('Back.index', ['latest_users' => $users->take(4), 'days' => getCollectionBerDayCount($users), 'users' => $users]);
    }

    public function trainings()
    {
        return view('Back.Trainings.index', ['trainings' => Training::all()]);
    }

    public function export(Request $request)
    {
        return Excel::download(new TrainingsExport, 'trainings.xlsx');
    }
}
