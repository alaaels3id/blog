<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\IPostRepository;

class PostController extends Controller
{
    protected $repo;

    public function __construct(IPostRepository $repository)
    {
        $this->repo = $repository;
    }

    public function index()
    {
        return view('front.posts.index', ['posts' => $this->repo->paginate()]);
    }

    public function show($id)
    {
        return view('front.posts.show', ['post' => $this->repo->find($id)]);
    }
}
