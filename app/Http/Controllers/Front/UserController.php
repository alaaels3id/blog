<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\IUserRepository;

class UserController extends Controller
{
    protected $userRepo;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    public function index()
    {
        return view('front.users.index', ['users' => $this->userRepo->paginate()]);
    }

    public function show($id)
    {
        return view('front.users.show', ['user' => $this->userRepo->find($id)]);
    }
}
