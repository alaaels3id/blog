<?php

namespace App\Http\Callbacks;

use App\Crud\ApiProcess;
use App\Crud\CrudProcess;

trait AppCallbacks
{
    public static function getAuth() : callable
    {
        return function ($view) {
            return $view->with('auth', auth()->user());
        };
    }

    public static function getAllRoutes() : callable
    {
        return function ($view) {
            return $view->with('getAllRoutes', getAllRoutes());
        };
    }

    public static function getHasPermission() : callable
    {
        return function ($route) {
            return permission_route_checker($route);
        };
    }

    public static function getApiFacade()
    {
        return function ($app) {
            return new ApiProcess();
        };
    }

    public static function getCrudFacade()
    {
        return function ($app) {
            return new CrudProcess();
        };
    }
}
