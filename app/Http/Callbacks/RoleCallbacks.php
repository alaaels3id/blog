<?php

namespace App\Http\Callbacks;

trait RoleCallbacks
{
    public static function getEditedSince() : callable
    {
        return function ($model){ return $model->created_at->diffForHumans(); };
    }

    public static function getEditedUpdatedAt() : callable
    {
        return function ($model){ return $model->updated_at->diffForHumans(); };
    }

    public static function checkIfSuperAdmin()
    {
        return function ($query){
            return auth()->guard('admin')->user()->id != 1 ? $query->where('id', '!=', 1) : $query;
        };
    }
}
