<?php

namespace App\Http\Callbacks;

trait UserCallbacks
{
    public static function getEditedSince() : callable
    {
        return function ($model){ return $model->created_at->diffForHumans(); };
    }

    public static function getEditedUpdatedAt() : callable
    {
        return function ($model){ return $model->updated_at->diffForHumans(); };
    }
}
