<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    public function handle($request, Closure $next)
    {
        Carbon::setLocale($request->header('lang'));

        if(!$request->user()->status && !$request->user()->active)
        {
            Auth::guard('api')->logout();

            return apiResponseWarning('عفوا تم ايقاف تفعيل هذا الحساب من قبل الادارة يرجي التواصل لحل المشكلة');
        }
        return $next($request);
    }
}
