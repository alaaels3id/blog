<?php

namespace App\Http\Requests\Back;

use App\Rules\IsAr;
use App\Rules\IsEn;
use Illuminate\Foundation\Http\FormRequest;

class CreateBannerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type'  => 'required|in:image,video',
            'name'  => 'required|file',
            'image' => 'required_if:type,video|mimes:png,jpg,jpeg',
        ];
    }

    public function messages()
    {
        return [
            'image.required_if' => trans('back.banner-create-image'),
        ];
    }
}
