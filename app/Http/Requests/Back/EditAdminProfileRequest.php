<?php

namespace App\Http\Requests\Back;

use App\Rules\EmailFormatChecker;
use Illuminate\Foundation\Http\FormRequest;

class EditAdminProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'  => 'required|string',
            'email' => ['required','email', new EmailFormatChecker(), 'unique:admins,email,'.auth()->id()],
            'phone' => 'nullable|numeric|digits_between:11,11',
            'image' => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];
    }
}
