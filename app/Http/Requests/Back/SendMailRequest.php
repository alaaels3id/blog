<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class SendMailRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'to'      => 'required|email',
            'subject' => 'required|string',
            'message' => 'required|string',
        ];
    }
}
