<?php

namespace App\Http\Requests\Back;

use App\Rules\IsAr;
use App\Rules\IsEn;
use Illuminate\Foundation\Http\FormRequest;

class CreateCourseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'link' => 'required|url'
        ];

        foreach (config('sitelangs.locales') as $lang => $name) {
            $rules[$lang.'.name'] = ['required', 'string', $lang == 'ar' ? new IsAr() : new IsEn()];
        }

        return $rules;
    }
}
