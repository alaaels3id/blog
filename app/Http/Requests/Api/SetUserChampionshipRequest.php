<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class SetUserChampionshipRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'championship_id'   => 'required|numeric|exists:championships,id',
            'category'          => 'required|string',
            'championship_type' => 'required|string',
            'gender'            => 'required_if:championship_type,kumite|string',
            'weight'            => 'required_if:championship_type,kumite|numeric',
        ];
    }

    public function messages()
    {
        return [
            'gender.required_if' => 'حقل النوع مطلوب',
            'weight.required_if' => 'حقل الوزن مطلوب',
        ];
    }
}
