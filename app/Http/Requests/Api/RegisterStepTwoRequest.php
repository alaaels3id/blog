<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class RegisterStepTwoRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'country_id' => 'required|numeric|exists:countries,id',
            'address'    => 'required|string',
            'age'        => 'required|numeric',
            'gender'     => 'required|in:male,female',
        ];
    }

    public function messages()
    {
        return [
            'password.min' => 'حقل كلمة المرور يجب ان لا يقل عن 6 أحرف',
        ];
    }
}
