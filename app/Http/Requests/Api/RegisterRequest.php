<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class RegisterRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'         => 'required|string',
            'email'        => ['required','email', new EmailFormatChecker(), 'unique:users,email'],
            'phone'        => 'required|unique:users,phone',
            'gender'       => 'required|in:male,female',
            'password'     => 'required|string|min:6',
            'device_token' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'password.min' => 'حقل كلمة المرور يجب ان لا يقل عن 6 أحرف',
        ];
    }
}
