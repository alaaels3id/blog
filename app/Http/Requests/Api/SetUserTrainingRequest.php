<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class SetUserTrainingRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'    => 'required|string',
            'belt'    => 'required|string',
            'agency'  => 'required|string',
        ];
    }
}
