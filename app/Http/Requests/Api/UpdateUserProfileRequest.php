<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class UpdateUserProfileRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = auth()->guard('api')->user()->id;

        return [
            'name'     => 'nullable|string',
            'email'    => ['nullable', 'email', new EmailFormatChecker(), 'unique:users,email,' . $id],
            'phone'    => 'nullable|numeric|unique:users,phone,' . $id,
            'identity' => 'nullable|numeric|unique:users,identity,' . $id,
            'gender'   => 'nullable|in:male,female',
            'image'    => 'nullable|image|mimes:jpg,png,jpeg',
        ];
    }
}
