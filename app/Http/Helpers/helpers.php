<?php

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

function str() { return new \Illuminate\Support\Str;}

function arr() { return new \Illuminate\Support\Arr;}

function carbon() { return new \Carbon\Carbon; }

function direction() { return app()->isLocale('ar') ? 'rtl' : 'ltr'; }

function floating($right, $left) { return app()->isLocale('ar') ? $right : $left; }

function translated($type, $obj)
{
    return trans('back.'.$type.'-done',['var' => trans('back.t-'.$obj)]);
}

function localeUrl($url, $locale = null) { return LaravelLocalization::getLocalizedURL($locale, $url); }

function getSetting($setting_name)
{
    return \App\Models\Setting::whereStatus(1)->where('key', $setting_name)->first()->value ?? null;
}

function str_limit_30($text) { return ucwords(str()->limit($text, 30, '...')); }

function isNullable($text){ return (!isset($text) || $text == null || $text == '') ? trans('back.no-value') : ucwords($text); }

function getTime($time) { return carbon()->createFromFormat('H:i', $time)->format('h:i A'); }

function setTimeByFormat($format1, $format2, $time) { return carbon()->createFromFormat($format1, $time)->format($format2); }

function create_rand_numbers($digits = 4){ return rand(pow(10, $digits-1), pow(10, $digits)-1); }

function setActive($url) { return request()->is(app()->getLocale().'/'.$url) ? 'active' : ''; }

function active($route) { return Route::is($route); }

function isLocalised($lang) { return LaravelLocalization::getLocalizedURL($lang); }

function get_settings_image($setting)
{
    return ($setting->value) ? url('public/uploaded/settings/' . $setting->value) : url('public/admin/img/avatar10.jpg');
}

function uploaded($img, $model)
{
    $filename =  $model . '_' . str()->random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

    if (!file_exists(public_path('uploaded/'.str()->plural($model).'/')))
        mkdir(public_path('uploaded/'.str()->plural($model).'/'), 0777, true);

    $path = public_path('uploaded/'.str()->plural($model).'/');

    Image::make($img)->save($path . $filename);

    return $filename;
}

function uploadMany($imgs, $model)
{
    $images = [];

    foreach($imgs as $key => $img) { $images[$key]['image'] = uploaded($img, $model); }

    return $images;
}

function getIcons($with_null = null)
{
    // warning this icons works only with fontawesome 4
    $cards  = $with_null == null ? [] : ['' => ''];
    $cards += [
        'fab fa-500px'                               => 'fab fa-500px',
        'fa fa-adjust'                               => 'fa fa-adjust',
        'fa fa-american-sign-language-interpreting'  => 'fa fa-american-sign-language-interpreting',
        'fab fa-adn'                                 => 'fab fa-adn',
        'fa fa-address-book'                         => 'fa fa-address-book',
        'fa fa-address-book-o'                       => 'fa fa-address-book-o',
        'fa fa-address-card'                         => 'fa fa-address-card',
        'fa fa-address-card-o'                       => 'fa fa-address-card-o',
        'fa fa-align-center'                         => 'fa fa-align-center',
        'fa fa-align-justify'                        => 'fa fa-align-justify',
        'fa fa-align-left'                           => 'fa fa-align-left',
        'fa fa-align-right'                          => 'fa fa-align-right',
        'fab fa-amazon'                              => 'fab fa-amazon',
        'fa fa-ambulance'                            => 'fa fa-ambulance',
        'fa fa-anchor'                               => 'fa fa-anchor',
        'fab fa-android'                             => 'fab fa-android',
        'fab fa-angellist'                           => 'fab fa-angellist',
        'fa fa-angle-double-down'                    => 'fa fa-angle-double-down',
        'fa fa-angle-double-left'                    => 'fa fa-angle-double-left',
        'fa fa-angle-double-right'                   => 'fa fa-angle-double-right',
        'fa fa-angle-double-up'                      => 'fa fa-angle-double-up',
        'fa fa-angle-down'                           => 'fa fa-angle-down',
        'fa fa-angle-left'                           => 'fa fa-angle-left',
        'fa fa-angle-right'                          => 'fa fa-angle-right',
        'fa fa-angle-up'                             => 'fa fa-angle-up',
        'fab fa-apple'                               => 'fab fa-apple',
        'fa fa-archive'                              => 'fa fa-archive',
        'fa fa-area-chart'                           => 'fa fa-area-chart',
        'fa fa-arrow-circle-down'                    => 'fa fa-arrow-circle-down',
        'fa fa-arrow-circle-left'                    => 'fa fa-arrow-circle-left',
        'fa fa-arrow-circle-o-down'                  => 'fa fa-arrow-circle-o-down',
        'fa fa-arrow-circle-o-left'                  => 'fa fa-arrow-circle-o-left',
        'fa fa-arrow-circle-o-right'                 => 'fa fa-arrow-circle-o-right',
        'fa fa-arrow-circle-o-up'                    => 'fa fa-arrow-circle-o-up',
        'fa fa-arrow-circle-right'                   => 'fa fa-arrow-circle-right',
        'fa fa-arrow-circle-up'                      => 'fa fa-arrow-circle-up',
        'fa fa-arrow-down'                           => 'fa fa-arrow-down',
        'fa fa-arrow-left'                           => 'fa fa-arrow-left',
        'fa fa-arrow-right'                          => 'fa fa-arrow-right',
        'fa fa-arrow-up'                             => 'fa fa-arrow-up',
        'fa fa-arrows-alt'                           => 'fa fa-arrows-alt',
        'fa fa-arrows-h'                             => 'fa fa-arrows-h',
        'fa fa-arrows-v'                             => 'fa fa-arrows-v',
        'fa fa-arrows'                               => 'fa fa-arrows',
        'fa fa-asl-interpreting'                     => 'fa fa-asl-interpreting',
        'fa fa-assistive-listening-systems'          => 'fa fa-assistive-listening-systems',
        'fa fa-asterisk'                             => 'fa fa-asterisk',
        'fa fa-audio-description'                    => 'fa fa-audio-description',
        'fa fa-automobile'                           => 'fa fa-automobile',
        'fa fa-backward'                             => 'fa fa-backward',
        'fa fa-balance-scale'                        => 'fa fa-balance-scale',
        'fa fa-ban'                                  => 'fa fa-ban',
        'fab fa-bandcamp'                            => 'fab fa-bandcamp',
        'fa fa-bank'                                 => 'fa fa-bank',
        'fa fa-bar-chart'                            => 'fa fa-bar-chart',
        'fa fa-bar-chart-o'                          => 'fa fa-bar-chart-o',
        'fa fa-barcode'                              => 'fa fa-barcode',
        'fa fa-bars'                                 => 'fa fa-bars',
        'fa fa-bath'                                 => 'fa fa-bath',
        'fa fa-bathtub'                              => 'fa fa-bathtub',
        'fa fa-battery-0'                            => 'fa fa-battery-0',
        'fa fa-battery-1'                            => 'fa fa-battery-1',
        'fa fa-battery-2'                            => 'fa fa-battery-2',
        'fa fa-battery-3'                            => 'fa fa-battery-3',
        'fa fa-battery-4'                            => 'fa fa-battery-4',
        'fa fa-battery-empty'                        => 'fa fa-battery-empty',
        'fa fa-battery-full'                         => 'fa fa-battery-full',
        'fa fa-battery-half'                         => 'fa fa-battery-half',
        'fa fa-battery-quarter'                      => 'fa fa-battery-quarter',
        'fa fa-battery-three-quarters'               => 'fa fa-battery-three-quarters',
        'fa fa-bed'                                  => 'fa fa-bed',
        'fa fa-beer'                                 => 'fa fa-beer',
        'fab fa-behance'                             => 'fab fa-behance',
        'fab fa-behance-square'                      => 'fab fa-behance-square',
        'fa fa-bell-o'                               => 'fa fa-bell-o',
        'fa fa-bell-slash'                           => 'fa fa-bell-slash',
        'fa fa-bell-slash-o'                         => 'fa fa-bell-slash-o',
        'fa fa-bicycle'                              => 'fa fa-bicycle',
        'fa fa-binoculars'                           => 'fa fa-binoculars',
        'fa fa-birthday-cake'                        => 'fa fa-birthday-cake',
        'fab fa-bitbucket'                           => 'fab fa-bitbucket',
        'fab fa-bitcoin'                             => 'fab fa-bitcoin',
        'fab fa-black-tie'                           => 'fab fa-black-tie',
        'fa fa-blind'                                => 'fa fa-blind',
        'fab fa-bluetooth'                           => 'fab fa-bluetooth',
        'fab fa-bluetooth-b'                         => 'fab fa-bluetooth-b',
        'fa fa-bold'                                 => 'fa fa-bold',
        'fa fa-bolt'                                 => 'fa fa-bolt',
        'fa fa-bomb'                                 => 'fa fa-bomb',
        'fa fa-book'                                 => 'fa fa-book',
        'fa fa-bookmark'                             => 'fa fa-bookmark',
        'fa fa-bookmark-o'                           => 'fa fa-bookmark-o',
        'fa fa-braille'                              => 'fa fa-braille',
        'fa fa-briefcase'                            => 'fa fa-briefcase',
        'fab fa-btc'                                 => 'fab fa-btc',
        'fa fa-bug'                                  => 'fa fa-bug',
        'fa fa-building'                             => 'fa fa-building',
        'fa fa-building-o'                           => 'fa fa-building-o',
        'fa fa-bullhorn'                             => 'fa fa-bullhorn',
        'fa fa-bullseye'                             => 'fa fa-bullseye',
        'fa fa-bus'                                  => 'fa fa-bus',
        'fab fa-buysellads'                          => 'fab fa-buysellads',
        'fa fa-cab'                                  => 'fa fa-cab',
        'fa fa-calculator'                           => 'fa fa-calculator',
        'fa fa-calendar'                             => 'fa fa-calendar',
        'fa fa-calendar-check-o'                     => 'fa fa-calendar-check-o',
        'fa fa-calendar-minus-o'                     => 'fa fa-calendar-minus-o',
        'fa fa-calendar-plus-o'                      => 'fa fa-calendar-plus-o',
        'fa fa-calendar-times-o'                     => 'fa fa-calendar-times-o',
        'fa fa-calendar-o'                           => 'fa fa-calendar-o',
        'fa fa-camera'                               => 'fa fa-camera',
        'fa fa-camera-retro'                         => 'fa fa-camera-retro',
        'fa fa-car'                                  => 'fa fa-car',
        'fa fa-caret-down'                           => 'fa fa-caret-down',
        'fa fa-caret-left'                           => 'fa fa-caret-left',
        'fa fa-caret-right'                          => 'fa fa-caret-right',
        'fa fa-caret-square-o-down'                  => 'fa fa-caret-square-o-down',
        'fa fa-caret-square-o-left'                  => 'fa fa-caret-square-o-left',
        'fa fa-caret-square-o-right'                 => 'fa fa-caret-square-o-right',
        'fa fa-caret-square-o-up'                    => 'fa fa-caret-square-o-up',
        'fa fa-caret-up'                             => 'fa fa-caret-up',
        'fa fa-cart-arrow-down'                      => 'fa fa-cart-arrow-down',
        'fa fa-cart-plus'                            => 'fa fa-cart-plus',
        'fa fa-cc'                                   => 'fa fa-cc',
        'fab fa-cc-amex'                             => 'fab fa-cc-amex',
        'fab fa-cc-diners-club'                      => 'fab fa-cc-diners-club',
        'fab fa-cc-discover'                         => 'fab fa-cc-discover',
        'fab fa-cc-jcb'                              => 'fab fa-cc-jcb',
        'fab fa-cc-mastercard'                       => 'fab fa-cc-mastercard',
        'fab fa-cc-paypal'                           => 'fab fa-cc-paypal',
        'fab fa-cc-stripe'                           => 'fab fa-cc-stripe',
        'fab fa-cc-visa'                             => 'fab fa-cc-visa',
        'fa fa-certificate'                          => 'fa fa-certificate',
        'fa fa-chain'                                => 'fa fa-chain',
        'fa fa-chain-broken'                         => 'fa fa-chain-broken',
        'fa fa-check'                                => 'fa fa-check',
        'fa fa-check-circle'                         => 'fa fa-check-circle',
        'fa fa-check-circle-o'                       => 'fa fa-check-circle-o',
        'fa fa-check-square'                         => 'fa fa-check-square',
        'fa fa-check-square-o'                       => 'fa fa-check-square-o',
        'fa fa-chevron-circle-down'                  => 'fa fa-chevron-circle-down',
        'fa fa-chevron-circle-left'                  => 'fa fa-chevron-circle-left',
        'fa fa-chevron-circle-right'                 => 'fa fa-chevron-circle-right',
        'fa fa-chevron-circle-up'                    => 'fa fa-chevron-circle-up',
        'fa fa-chevron-down'                         => 'fa fa-chevron-down',
        'fa fa-chevron-left'                         => 'fa fa-chevron-left',
        'fa fa-chevron-right'                        => 'fa fa-chevron-right',
        'fa fa-chevron-up'                           => 'fa fa-chevron-up',
        'fa fa-child'                                => 'fa fa-child',
        'fa fa-chrome'                               => 'fa fa-chrome',
        'fa fa-clone'                                => 'fa fa-clone',
        'fa fa-circle'                               => 'fa fa-circle',
        'fa fa-circle-o'                             => 'fa fa-circle-o',
        'fa fa-circle-o-notch'                       => 'fa fa-circle-o-notch',
        'fa fa-circle-thin'                          => 'fa fa-circle-thin',
        'fa fa-clipboard'                            => 'fa fa-clipboard',
        'fa fa-clock-o'                              => 'fa fa-clock-o',
        'fa fa-close'                                => 'fa fa-close',
        'fa fa-cloud'                                => 'fa fa-cloud',
        'fa fa-cloud-download'                       => 'fa fa-cloud-download',
        'fa fa-cloud-upload'                         => 'fa fa-cloud-upload',
        'fa fa-cny'                                  => 'fa fa-cny',
        'fa fa-code'                                 => 'fa fa-code',
        'fa fa-code-fork'                            => 'fa fa-code-fork',
        'fab fa-codepen'                             => 'fab fa-codepen',
        'fab fa-codiepie'                            => 'fab fa-codiepie',
        'fa fa-coffee'                               => 'fa fa-coffee',
        'fa fa-cog'                                  => 'fa fa-cog',
        'fa fa-cogs'                                 => 'fa fa-cogs',
        'fa fa-columns'                              => 'fa fa-columns',
        'fa fa-comment'                              => 'fa fa-comment',
        'fa fa-comment-o'                            => 'fa fa-comment-o',
        'fa fa-comments'                             => 'fa fa-comments',
        'fa fa-comments-o'                           => 'fa fa-comments-o',
        'fa fa-commenting'                           => 'fa fa-commenting',
        'fa fa-commenting-o'                         => 'fa fa-commenting-o',
        'fa fa-compass'                              => 'fa fa-compass',
        'fa fa-compress'                             => 'fa fa-compress',
        'fab fa-connectdevelop'                      => 'fab fa-connectdevelop',
        'fab fa-contao'                              => 'fab fa-contao',
        'fa fa-copy'                                 => 'fa fa-copy',
        'fa fa-copyright'                            => 'fa fa-copyright',
        'fab fa-creative-commons'                    => 'fab fa-creative-commons',
        'fa fa-credit-card'                          => 'fa fa-credit-card',
        'fa fa-credit-card-alt'                      => 'fa fa-credit-card-alt',
        'fa fa-crop'                                 => 'fa fa-crop',
        'fa fa-crosshairs'                           => 'fa fa-crosshairs',
        'fab fa-css3'                                => 'fab fa-css3',
        'fa fa-cube'                                 => 'fa fa-cube',
        'fa fa-cubes'                                => 'fa fa-cubes',
        'fa fa-cut'                                  => 'fa fa-cut',
        'fa fa-cutlery'                              => 'fa fa-cutlery',
        'fa fa-dashboard'                            => 'fa fa-dashboard',
        'fab fa-dashcube'                            => 'fab fa-dashcube',
        'fa fa-database'                             => 'fa fa-database',
        'fa fa-deaf'                                 => 'fa fa-deaf',
        'fa fa-deafness'                             => 'fa fa-deafness',
        'fa fa-dedent'                               => 'fa fa-dedent',
        'fa fa-delicious'                            => 'fa fa-delicious',
        'fa fa-desktop'                              => 'fa fa-desktop',
        'fa fa-deviantart'                           => 'fa fa-deviantart',
        'fa fa-diamond'                              => 'fa fa-diamond',
        'fa fa-digg'                                 => 'fa fa-digg',
        'fa fa-dollar'                               => 'fa fa-dollar',
        'fa fa-dot-circle-o'                         => 'fa fa-dot-circle-o',
        'fa fa-download'                             => 'fa fa-download',
        'fa fa-dribbble'                             => 'fa fa-dribbble',
        'fa fa-drivers-license'                      => 'fa fa-drivers-license',
        'fa fa-drivers-license-o'                    => 'fa fa-drivers-license-o',
        'fa fa-dropbox'                              => 'fa fa-dropbox',
        'fa fa-drupal'                               => 'fa fa-drupal',
        'fa fa-edge'                                 => 'fa fa-edge',
        'fa fa-edit'                                 => 'fa fa-edit',
        'fa fa-eercast'                              => 'fa fa-eercast',
        'fa fa-eject'                                => 'fa fa-eject',
        'fa fa-ellipsis-h'                           => 'fa fa-ellipsis-h',
        'fa fa-ellipsis-v'                           => 'fa fa-ellipsis-v',
        'fa fa-empire'                               => 'fa fa-empire',
        'fa fa-envelope'                             => 'fa fa-envelope',
        'fa fa-envelope-o'                           => 'fa fa-envelope-o',
        'fa fa-envelope-open'                        => 'fa fa-envelope-open',
        'fa fa-envelope-open-o'                      => 'fa fa-envelope-open-o',
        'fa fa-envelope-square'                      => 'fa fa-envelope-square',
        'fa fa-envira'                               => 'fa fa-envira',
        'fa fa-eraser'                               => 'fa fa-eraser',
        'fa fa-etsy'                                 => 'fa fa-etsy',
        'fa fa-eur'                                  => 'fa fa-eur',
        'fa fa-euro'                                 => 'fa fa-euro',
        'fa fa-exchange'                             => 'fa fa-exchange',
        'fa fa-exclamation'                          => 'fa fa-exclamation',
        'fa fa-exclamation-circle'                   => 'fa fa-exclamation-circle',
        'fa fa-exclamation-triangle'                 => 'fa fa-exclamation-triangle',
        'fa fa-expand'                               => 'fa fa-expand',
        'fa fa-expeditedssl'                         => 'fa fa-expeditedssl',
        'fa fa-external-link'                        => 'fa fa-external-link',
        'fa fa-external-link-square'                 => 'fa fa-external-link-square',
        'fa fa-eye'                                  => 'fa fa-eye',
        'fa fa-eye-slash'                            => 'fa fa-eye-slash',
        'fa fa-eyedropper'                           => 'fa fa-eyedropper',
        'fa fa-facebook'                             => 'fa fa-facebook',
        'fa fa-facebook-f'                           => 'fa fa-facebook-f',
        'fa fa-facebook-official'                    => 'fa fa-facebook-official',
        'fa fa-facebook-square'                      => 'fa fa-facebook-square',
        'fa fa-fast-backward'                        => 'fa fa-fast-backward',
        'fa fa-fast-forward'                         => 'fa fa-fast-forward',
        'fa fa-fax'                                  => 'fa fa-fax',
        'fa fa-female'                               => 'fa fa-female',
        'fa fa-fighter-jet'                          => 'fa fa-fighter-jet',
        'fa fa-file'                                 => 'fa fa-file',
        'fa fa-file-archive-o'                       => 'fa fa-file-archive-o',
        'fa fa-file-audio-o'                         => 'fa fa-file-audio-o',
        'fa fa-file-code-o'                          => 'fa fa-file-code-o',
        'fa fa-file-excel-o'                         => 'fa fa-file-excel-o',
        'fa fa-file-image-o'                         => 'fa fa-file-image-o',
        'fa fa-file-movie-o'                         => 'fa fa-file-movie-o',
        'fa fa-file-o'                               => 'fa fa-file-o',
        'fa fa-file-pdf-o'                           => 'fa fa-file-pdf-o',
        'fa fa-file-photo-o'                         => 'fa fa-file-photo-o',
        'fa fa-file-picture-o'                       => 'fa fa-file-picture-o',
        'fa fa-file-powerpoint-o'                    => 'fa fa-file-powerpoint-o',
        'fa fa-file-sound-o'                         => 'fa fa-file-sound-o',
        'fa fa-file-text'                            => 'fa fa-file-text',
        'fa fa-file-text-o'                          => 'fa fa-file-text-o',
        'fa fa-file-video-o'                         => 'fa fa-file-video-o',
        'fa fa-file-word-o'                          => 'fa fa-file-word-o',
        'fa fa-file-zip-o'                           => 'fa fa-file-zip-o',
        'fa fa-files-o'                              => 'fa fa-files-o',
        'fa fa-film'                                 => 'fa fa-film',
        'fa fa-filter'                               => 'fa fa-filter',
        'fa fa-fire'                                 => 'fa fa-fire',
        'fa fa-fire-extinguisher'                    => 'fa fa-fire-extinguisher',
        'fa fa-firefox'                              => 'fa fa-firefox',
        'fa fa-first-order'                          => 'fa fa-first-order',
        'fa fa-flag'                                 => 'fa fa-flag',
        'fa fa-flag-checkered'                       => 'fa fa-flag-checkered',
        'fa fa-flag-o'                               => 'fa fa-flag-o',
        'fa fa-flash'                                => 'fa fa-flash',
        'fa fa-flask'                                => 'fa fa-flask',
        'fa fa-flickr'                               => 'fa fa-flickr',
        'fa fa-floppy-o'                             => 'fa fa-floppy-o',
        'fa fa-folder'                               => 'fa fa-folder',
        'fa fa-folder-o'                             => 'fa fa-folder-o',
        'fa fa-folder-open'                          => 'fa fa-folder-open',
        'fa fa-folder-open-o'                        => 'fa fa-folder-open-o',
        'fa fa-font'                                 => 'fa fa-font',
        'fa fa-fonticons'                            => 'fa fa-fonticons',
        'fa fa-fort-awesome'                         => 'fa fa-fort-awesome',
        'fa fa-forumbee'                             => 'fa fa-forumbee',
        'fa fa-forward'                              => 'fa fa-forward',
        'fa fa-foursquare'                           => 'fa fa-foursquare',
        'fa fa-free-code-camp'                       => 'fa fa-free-code-camp',
        'fa fa-frown-o'                              => 'fa fa-frown-o',
        'fa fa-futbol-o'                             => 'fa fa-futbol-o',
        'fa fa-gamepad'                              => 'fa fa-gamepad',
        'fa fa-gavel'                                => 'fa fa-gavel',
        'fa fa-gbp'                                  => 'fa fa-gbp',
        'fa fa-ge'                                   => 'fa fa-ge',
        'fa fa-gear'                                 => 'fa fa-gear',
        'fa fa-gears'                                => 'fa fa-gears',
        'fa fa-genderless'                           => 'fa fa-genderless',
        'fa fa-get-pocket'                           => 'fa fa-get-pocket',
        'fa fa-gg'                                   => 'fa fa-gg',
        'fa fa-gg-circle'                            => 'fa fa-gg-circle',
        'fa fa-gift'                                 => 'fa fa-gift',
        'fa fa-git'                                  => 'fa fa-git',
        'fa fa-git-square'                           => 'fa fa-git-square',
        'fa fa-github'                               => 'fa fa-github',
        'fa fa-github-alt'                           => 'fa fa-github-alt',
        'fa fa-github-square'                        => 'fa fa-github-square',
        'fa fa-gitlab'                               => 'fa fa-gitlab',
        'fa fa-gittip'                               => 'fa fa-gittip',
        'fa fa-glass'                                => 'fa fa-glass',
        'fa fa-glide'                                => 'fa fa-glide',
        'fa fa-glide-g'                              => 'fa fa-glide-g',
        'fa fa-globe'                                => 'fa fa-globe',
        'fa fa-google'                               => 'fa fa-google',
        'fa fa-google-plus'                          => 'fa fa-google-plus',
        'fa fa-google-plus-square'                   => 'fa fa-google-plus-square',
        'fa fa-google-wallet'                        => 'fa fa-google-wallet',
        'fa fa-graduation-cap'                       => 'fa fa-graduation-cap',
        'fa fa-gratipay'                             => 'fa fa-gratipay',
        'fa fa-grav'                                 => 'fa fa-grav',
        'fa fa-group'                                => 'fa fa-group',
        'fa fa-h-square'                             => 'fa fa-h-square',
        'fa fa-hacker-news'                          => 'fa fa-hacker-news',
        'fa fa-hand-grab-o'                          => 'fa fa-hand-grab-o',
        'fa fa-hand-lizard-o'                        => 'fa fa-hand-lizard-o',
        'fa fa-hand-paper-o'                         => 'fa fa-hand-paper-o',
        'fa fa-hand-peace-o'                         => 'fa fa-hand-peace-o',
        'fa fa-hand-pointer-o'                       => 'fa fa-hand-pointer-o',
        'fa fa-hand-rock-o'                          => 'fa fa-hand-rock-o',
        'fa fa-hand-scissors-o'                      => 'fa fa-hand-scissors-o',
        'fa fa-hand-spock-o'                         => 'fa fa-hand-spock-o',
        'fa fa-hand-stop-o'                          => 'fa fa-hand-stop-o',
        'fa fa-hand-o-down'                          => 'fa fa-hand-o-down',
        'fa fa-hand-o-left'                          => 'fa fa-hand-o-left',
        'fa fa-hand-o-right'                         => 'fa fa-hand-o-right',
        'fa fa-hand-o-up'                            => 'fa fa-hand-o-up',
        'fa fa-handshake-o'                          => 'fa fa-handshake-o',
        'fa fa-hard-of-hearing'                      => 'fa fa-hard-of-hearing',
        'fa fa-hashtag'                              => 'fa fa-hashtag',
        'fa fa-hdd-o'                                => 'fa fa-hdd-o',
        'fa fa-header'                               => 'fa fa-header',
        'fa fa-headphones'                           => 'fa fa-headphones',
        'fa fa-heart'                                => 'fa fa-heart',
        'fa fa-heart-o'                              => 'fa fa-heart-o',
        'fa fa-heartbeat'                            => 'fa fa-heartbeat',
        'fa fa-history'                              => 'fa fa-history',
        'fa fa-home'                                 => 'fa fa-home',
        'fa fa-hospital-o'                           => 'fa fa-hospital-o',
        'fa fa-hotel'                                => 'fa fa-hotel',
        'fa fa-hourglass'                            => 'fa fa-hourglass',
        'fa fa-hourglass-1'                          => 'fa fa-hourglass-1',
        'fa fa-hourglass-2'                          => 'fa fa-hourglass-2',
        'fa fa-hourglass-3'                          => 'fa fa-hourglass-3',
        'fa fa-hourglass-end'                        => 'fa fa-hourglass-end',
        'fa fa-hourglass-half'                       => 'fa fa-hourglass-half',
        'fa fa-hourglass-o'                          => 'fa fa-hourglass-o',
        'fa fa-hourglass-start'                      => 'fa fa-hourglass-start',
        'fa fa-houzz'                                => 'fa fa-houzz',
        'fa fa-html5'                                => 'fa fa-html5',
        'fa fa-i-cursor'                             => 'fa fa-i-cursor',
        'fa fa-id-badge'                             => 'fa fa-id-badge',
        'fa fa-id-card'                              => 'fa fa-id-card',
        'fa fa-id-card-o'                            => 'fa fa-id-card-o',
        'fa fa-ils'                                  => 'fa fa-ils',
        'fa fa-image'                                => 'fa fa-image',
        'fa fa-imdb'                                 => 'fa fa-imdb',
        'fa fa-inbox'                                => 'fa fa-inbox',
        'fa fa-indent'                               => 'fa fa-indent',
        'fa fa-industry'                             => 'fa fa-industry',
        'fa fa-info'                                 => 'fa fa-info',
        'fa fa-info-circle'                          => 'fa fa-info-circle',
        'fa fa-inr'                                  => 'fa fa-inr',
        'fa fa-instagram'                            => 'fa fa-instagram',
        'fa fa-institution'                          => 'fa fa-institution',
        'fa fa-internet-explorer'                    => 'fa fa-internet-explorer',
        'fa fa-ioxhost'                              => 'fa fa-ioxhost',
        'fa fa-italic'                               => 'fa fa-italic',
        'fa fa-joomla'                               => 'fa fa-joomla',
        'fa fa-jpy'                                  => 'fa fa-jpy',
        'fa fa-jsfiddle'                             => 'fa fa-jsfiddle',
        'fa fa-key'                                  => 'fa fa-key',
        'fa fa-keyboard-o'                           => 'fa fa-keyboard-o',
        'fa fa-krw'                                  => 'fa fa-krw',
        'fa fa-language'                             => 'fa fa-language',
        'fa fa-laptop'                               => 'fa fa-laptop',
        'fa fa-lastfm'                               => 'fa fa-lastfm',
        'fa fa-lastfm-square'                        => 'fa fa-lastfm-square',
        'fa fa-leaf'                                 => 'fa fa-leaf',
        'fa fa-leanpub'                              => 'fa fa-leanpub',
        'fa fa-legal'                                => 'fa fa-legal',
        'fa fa-lemon-o'                              => 'fa fa-lemon-o',
        'fa fa-level-down'                           => 'fa fa-level-down',
        'fa fa-level-up'                             => 'fa fa-level-up',
        'fa fa-life-bouy'                            => 'fa fa-life-bouy',
        'fa fa-life-buoy'                            => 'fa fa-life-buoy',
        'fa fa-life-ring'                            => 'fa fa-life-ring',
        'fa fa-life-saver'                           => 'fa fa-life-saver',
        'fa fa-lightbulb-o'                          => 'fa fa-lightbulb-o',
        'fa fa-line-chart'                           => 'fa fa-line-chart',
        'fa fa-link'                                 => 'fa fa-link',
        'fa fa-linkedin'                             => 'fa fa-linkedin',
        'fa fa-linkedin-square'                      => 'fa fa-linkedin-square',
        'fa fa-linode'                               => 'fa fa-linode',
        'fa fa-linux'                                => 'fa fa-linux',
        'fa fa-list-alt'                             => 'fa fa-list-alt',
        'fa fa-list-ol'                              => 'fa fa-list-ol',
        'fa fa-list-ul'                              => 'fa fa-list-ul',
        'fa fa-location-arrow'                       => 'fa fa-location-arrow',
        'fa fa-lock'                                 => 'fa fa-lock',
        'fa fa-long-arrow-down'                      => 'fa fa-long-arrow-down',
        'fa fa-long-arrow-left'                      => 'fa fa-long-arrow-left',
        'fa fa-long-arrow-right'                     => 'fa fa-long-arrow-right',
        'fa fa-long-arrow-up'                        => 'fa fa-long-arrow-up',
        'fa fa-low-vision'                           => 'fa fa-low-vision',
        'fa fa-magic'                                => 'fa fa-magic',
        'fa fa-magnet'                               => 'fa fa-magnet',
        'fa fa-mail-forward'                         => 'fa fa-mail-forward',
        'fa fa-mail-reply'                           => 'fa fa-mail-reply',
        'fa fa-mail-reply-all'                       => 'fa fa-mail-reply-all',
        'fa fa-male'                                 => 'fa fa-male',
        'fa fa-map'                                  => 'fa fa-map',
        'fa fa-map-marker'                           => 'fa fa-map-marker',
        'fa fa-map-o'                                => 'fa fa-map-o',
        'fa fa-map-pin'                              => 'fa fa-map-pin',
        'fa fa-map-signs'                            => 'fa fa-map-signs',
        'fa fa-mars'                                 => 'fa fa-mars',
        'fa fa-mars-double'                          => 'fa fa-mars-double',
        'fa fa-mars-stroke'                          => 'fa fa-mars-stroke',
        'fa fa-mars-stroke-h'                        => 'fa fa-mars-stroke-h',
        'fa fa-mars-stroke-v'                        => 'fa fa-mars-stroke-v',
        'fa fa-maxcdn'                               => 'fa fa-maxcdn',
        'fa fa-meanpath'                             => 'fa fa-meanpath',
        'fa fa-medium'                               => 'fa fa-medium',
        'fa fa-medkit'                               => 'fa fa-medkit',
        'fa fa-meetup'                               => 'fa fa-meetup',
        'fa fa-meh-o'                                => 'fa fa-meh-o',
        'fa fa-mercury'                              => 'fa fa-mercury',
        'fa fa-microchip'                            => 'fa fa-microchip',
        'fa fa-microphone'                           => 'fa fa-microphone',
        'fa fa-microphone-slash'                     => 'fa fa-microphone-slash',
        'fa fa-minus'                                => 'fa fa-minus',
        'fa fa-minus-circle'                         => 'fa fa-minus-circle',
        'fa fa-minus-square'                         => 'fa fa-minus-square',
        'fa fa-minus-square-o'                       => 'fa fa-minus-square-o',
        'fa fa-mixcloud'                             => 'fa fa-mixcloud',
        'fa fa-mobile'                               => 'fa fa-mobile',
        'fa fa-mobile-phone'                         => 'fa fa-mobile-phone',
        'fa fa-modx'                                 => 'fa fa-modx',
        'fa fa-money'                                => 'fa fa-money',
        'fa fa-moon-o'                               => 'fa fa-moon-o',
        'fa fa-mortar-board'                         => 'fa fa-mortar-board',
        'fa fa-motorcycle'                           => 'fa fa-motorcycle',
        'fa fa-mouse-pointer'                        => 'fa fa-mouse-pointer',
        'fa fa-music'                                => 'fa fa-music',
        'fa fa-navicon'                              => 'fa fa-navicon',
        'fa fa-neuter'                               => 'fa fa-neuter',
        'fa fa-newspaper-o'                          => 'fa fa-newspaper-o',
        'fa fa-object-group'                         => 'fa fa-object-group',
        'fa fa-object-ungroup'                       => 'fa fa-object-ungroup',
        'fa fa-odnoklassniki'                        => 'fa fa-odnoklassniki',
        'fa fa-odnoklassniki-square'                 => 'fa fa-odnoklassniki-square',
        'fa fa-opencart'                             => 'fa fa-opencart',
        'fa fa-openid'                               => 'fa fa-openid',
        'fa fa-opera'                                => 'fa fa-opera',
        'fa fa-optin-monster'                        => 'fa fa-optin-monster',
        'fa fa-outdent'                              => 'fa fa-outdent',
        'fa fa-pagelines'                            => 'fa fa-pagelines',
        'fa fa-paint-brush'                          => 'fa fa-paint-brush',
        'fa fa-paper-plane'                          => 'fa fa-paper-plane',
        'fa fa-paper-plane-o'                        => 'fa fa-paper-plane-o',
        'fa fa-paperclip'                            => 'fa fa-paperclip',
        'fa fa-paragraph'                            => 'fa fa-paragraph',
        'fa fa-paste'                                => 'fa fa-paste',
        'fa fa-pause'                                => 'fa fa-pause',
        'fa fa-pause-circle'                         => 'fa fa-pause-circle',
        'fa fa-pause-circle-o'                       => 'fa fa-pause-circle-o',
        'fa fa-paw'                                  => 'fa fa-paw',
        'fa fa-paypal'                               => 'fa fa-paypal',
        'fa fa-pencil'                               => 'fa fa-pencil',
        'fa fa-pencil-square'                        => 'fa fa-pencil-square',
        'fa fa-pencil-square-o'                      => 'fa fa-pencil-square-o',
        'fa fa-percent'                              => 'fa fa-percent',
        'fa fa-phone'                                => 'fa fa-phone',
        'fa fa-phone-square'                         => 'fa fa-phone-square',
        'fa fa-photo'                                => 'fa fa-photo',
        'fa fa-picture-o'                            => 'fa fa-picture-o',
        'fa fa-pie-chart'                            => 'fa fa-pie-chart',
        'fa fa-pied-piper'                           => 'fa fa-pied-piper',
        'fa fa-pied-piper-alt'                       => 'fa fa-pied-piper-alt',
        'fa fa-pinterest'                            => 'fa fa-pinterest',
        'fa fa-pinterest-p'                          => 'fa fa-pinterest-p',
        'fa fa-pinterest-square'                     => 'fa fa-pinterest-square',
        'fa fa-plane'                                => 'fa fa-plane',
        'fa fa-play'                                 => 'fa fa-play',
        'fa fa-play-circle'                          => 'fa fa-play-circle',
        'fa fa-play-circle-o'                        => 'fa fa-play-circle-o',
        'fa fa-plug'                                 => 'fa fa-plug',
        'fa fa-plus'                                 => 'fa fa-plus',
        'fa fa-plus-circle'                          => 'fa fa-plus-circle',
        'fa fa-plus-square'                          => 'fa fa-plus-square',
        'fa fa-plus-square-o'                        => 'fa fa-plus-square-o',
        'fa fa-podcast'                              => 'fa fa-podcast',
        'fa fa-power-off'                            => 'fa fa-power-off',
        'fa fa-print'                                => 'fa fa-print',
        'fa fa-product-hunt'                         => 'fa fa-product-hunt',
        'fa fa-puzzle-piece'                         => 'fa fa-puzzle-piece',
        'fa fa-qq'                                   => 'fa fa-qq',
        'fa fa-qrcode'                               => 'fa fa-qrcode',
        'fa fa-question'                             => 'fa fa-question',
        'fa fa-question-circle'                      => 'fa fa-question-circle',
        'fa fa-question-circle-o'                    => 'fa fa-question-circle-o',
        'fa fa-quora'                                => 'fa fa-quora',
        'fa fa-quote-left'                           => 'fa fa-quote-left',
        'fa fa-quote-right'                          => 'fa fa-quote-right',
        'fa fa-ra'                                   => 'fa fa-ra',
        'fa fa-random'                               => 'fa fa-random',
        'fa fa-ravelry'                              => 'fa fa-ravelry',
        'fa fa-rebel'                                => 'fa fa-rebel',
        'fa fa-recycle'                              => 'fa fa-recycle',
        'fa fa-reddit'                               => 'fa fa-reddit',
        'fa fa-reddit-alien'                         => 'fa fa-reddit-alien',
        'fa fa-reddit-square'                        => 'fa fa-reddit-square',
        'fa fa-refresh'                              => 'fa fa-refresh',
        'fa fa-registered'                           => 'fa fa-registered',
        'fa fa-remove'                               => 'fa fa-remove',
        'fa fa-renren'                               => 'fa fa-renren',
        'fa fa-reorder'                              => 'fa fa-reorder',
        'fa fa-repeat'                               => 'fa fa-repeat',
        'fa fa-reply'                                => 'fa fa-reply',
        'fa fa-reply-all'                            => 'fa fa-reply-all',
        'fa fa-retweet'                              => 'fa fa-retweet',
        'fa fa-rmb'                                  => 'fa fa-rmb',
        'fa fa-road'                                 => 'fa fa-road',
        'fa fa-rocket'                               => 'fa fa-rocket',
        'fa fa-rouble'                               => 'fa fa-rouble',
        'fa fa-s15'                                  => 'fa fa-s15',
        'fa fa-safari'                               => 'fa fa-safari',
        'fa fa-save'                                 => 'fa fa-save',
        'fa fa-scissors'                             => 'fa fa-scissors',
        'fa fa-scribd'                               => 'fa fa-scribd',
        'fa fa-search'                               => 'fa fa-search',
        'fa fa-search-minus'                         => 'fa fa-search-minus',
        'fa fa-search-plus'                          => 'fa fa-search-plus',
        'fa fa-sellsy'                               => 'fa fa-sellsy',
        'fa fa-send'                                 => 'fa fa-send',
        'fa fa-send-o'                               => 'fa fa-send-o',
        'fa fa-server'                               => 'fa fa-server',
        'fa fa-share'                                => 'fa fa-share',
        'fa fa-share-alt'                            => 'fa fa-share-alt',
        'fa fa-share-alt-square'                     => 'fa fa-share-alt-square',
        'fa fa-share-square'                         => 'fa fa-share-square',
        'fa fa-shower'                               => 'fa fa-shower',
        'fa fa-square-o'                             => 'fa fa-square-o',
        'fa fa-share-square-o'                       => 'fa fa-share-square-o',
        'fa fa-shekel'                               => 'fa fa-shekel',
        'fa fa-sheqel'                               => 'fa fa-sheqel',
        'fa fa-shield'                               => 'fa fa-shield',
        'fa fa-ship'                                 => 'fa fa-ship',
        'fa fa-shirtsinbulk'                         => 'fa fa-shirtsinbulk',
        'fa fa-shopping-bag'                         => 'fa fa-shopping-bag',
        'fa fa-shopping-basket'                      => 'fa fa-shopping-basket',
        'fa fa-shopping-cart'                        => 'fa fa-shopping-cart',
        'fa fa-sign-in'                              => 'fa fa-sign-in',
        'fa fa-sign-out'                             => 'fa fa-sign-out',
        'fa fa-signal'                               => 'fa fa-signal',
        'fa fa-sign-language'                        => 'fa fa-sign-language',
        'fa fa-signing'                              => 'fa fa-signing',
        'fa fa-simplybuilt'                          => 'fa fa-simplybuilt',
        'fa fa-sitemap'                              => 'fa fa-sitemap',
        'fa fa-skyatlas'                             => 'fa fa-skyatlas',
        'fa fa-skype'                                => 'fa fa-skype',
        'fa fa-slack'                                => 'fa fa-slack',
        'fa fa-sliders'                              => 'fa fa-sliders',
        'fa fa-slideshare'                           => 'fa fa-slideshare',
        'fa fa-smile-o'                              => 'fa fa-smile-o',
        'fa fa-snapchat'                             => 'fa fa-snapchat',
        'fa fa-snapchat-ghost'                       => 'fa fa-snapchat-ghost',
        'fa fa-snapchat-square'                      => 'fa fa-snapchat-square',
        'fa fa-snowflake-o'                          => 'fa fa-snowflake-o',
        'fa fa-soccer-ball-o'                        => 'fa fa-soccer-ball-o',
        'fa fa-sort'                                 => 'fa fa-sort',
        'fa fa-sort-alpha-asc'                       => 'fa fa-sort-alpha-asc',
        'fa fa-sort-alpha-desc'                      => 'fa fa-sort-alpha-desc',
        'fa fa-sort-amount-asc'                      => 'fa fa-sort-amount-asc',
        'fa fa-sort-amount-desc'                     => 'fa fa-sort-amount-desc',
        'fa fa-sort-asc'                             => 'fa fa-sort-asc',
        'fa fa-sort-desc'                            => 'fa fa-sort-desc',
        'fa fa-sort-down'                            => 'fa fa-sort-down',
        'fa fa-sort-numeric-asc'                     => 'fa fa-sort-numeric-asc',
        'fa fa-sort-numeric-desc'                    => 'fa fa-sort-numeric-desc',
        'fa fa-sort-up'                              => 'fa fa-sort-up',
        'fa fa-soundcloud'                           => 'fa fa-soundcloud',
        'fa fa-space-shuttle'                        => 'fa fa-space-shuttle',
        'fa fa-spinner'                              => 'fa fa-spinner',
        'fa fa-spoon'                                => 'fa fa-spoon',
        'fa fa-spotify'                              => 'fa fa-spotify',
        'fa fa-square'                               => 'fa fa-square',
        'fa fa-stack-exchange'                       => 'fa fa-stack-exchange',
        'fa fa-stack-overflow'                       => 'fa fa-stack-overflow',
        'fa fa-star'                                 => 'fa fa-star',
        'fa fa-star-half'                            => 'fa fa-star-half',
        'fa fa-star-half-empty'                      => 'fa fa-star-half-empty',
        'fa fa-star-half-full'                       => 'fa fa-star-half-full',
        'fa fa-star-half-o'                          => 'fa fa-star-half-o',
        'fa fa-star-o'                               => 'fa fa-star-o',
        'fa fa-steam'                                => 'fa fa-steam',
        'fa fa-steam-square'                         => 'fa fa-steam-square',
        'fa fa-step-backward'                        => 'fa fa-step-backward',
        'fa fa-step-forward'                         => 'fa fa-step-forward',
        'fa fa-stethoscope'                          => 'fa fa-stethoscope',
        'fa fa-sticky-note'                          => 'fa fa-sticky-note',
        'fa fa-sticky-note-o'                        => 'fa fa-sticky-note-o',
        'fa fa-stop'                                 => 'fa fa-stop',
        'fa fa-stop-circle'                          => 'fa fa-stop-circle',
        'fa fa-stop-circle-o'                        => 'fa fa-stop-circle-o',
        'fa fa-street-view'                          => 'fa fa-street-view',
        'fa fa-strikethrough'                        => 'fa fa-strikethrough',
        'fa fa-stumbleupon'                          => 'fa fa-stumbleupon',
        'fa fa-stumbleupon-circle'                   => 'fa fa-stumbleupon-circle',
        'fa fa-subscript'                            => 'fa fa-subscript',
        'fa fa-subway'                               => 'fa fa-subway',
        'fa fa-suitcase'                             => 'fa fa-suitcase',
        'fa fa-sun-o'                                => 'fa fa-sun-o',
        'fa fa-superpowers'                          => 'fa fa-superpowers',
        'fa fa-superscript'                          => 'fa fa-superscript',
        'fa fa-support'                              => 'fa fa-support',
        'fa fa-table'                                => 'fa fa-table',
        'fa fa-tablet'                               => 'fa fa-tablet',
        'fa fa-tachometer'                           => 'fa fa-tachometer',
        'fa fa-tag'                                  => 'fa fa-tag',
        'fa fa-tags'                                 => 'fa fa-tags',
        'fa fa-tasks'                                => 'fa fa-tasks',
        'fa fa-taxi'                                 => 'fa fa-taxi',
        'fa fa-telegram'                             => 'fa fa-telegram',
        'fa fa-television'                           => 'fa fa-television',
        'fa fa-tencent-weibo'                        => 'fa fa-tencent-weibo',
        'fa fa-terminal'                             => 'fa fa-terminal',
        'fa fa-text-height'                          => 'fa fa-text-height',
        'fa fa-text-width'                           => 'fa fa-text-width',
        'fa fa-th'                                   => 'fa fa-th',
        'fa fa-th-large'                             => 'fa fa-th-large',
        'fa fa-th-list'                              => 'fa fa-th-list',
        'fa fa-themeisle'                            => 'fa fa-themeisle',
        'fa fa-thermometer'                          => 'fa fa-thermometer',
        'fa fa-thermometer-0'                        => 'fa fa-thermometer-0',
        'fa fa-thermometer-1'                        => 'fa fa-thermometer-1',
        'fa fa-thermometer-2'                        => 'fa fa-thermometer-2',
        'fa fa-thermometer-3'                        => 'fa fa-thermometer-3',
        'fa fa-thermometer-4'                        => 'fa fa-thermometer-4',
        'fa fa-thermometer-empty'                    => 'fa fa-thermometer-empty',
        'fa fa-thermometer-full'                     => 'fa fa-thermometer-full',
        'fa fa-thermometer-half'                     => 'fa fa-thermometer-half',
        'fa fa-thermometer-quarter'                  => 'fa fa-thermometer-quarter',
        'fa fa-thermometer-three-quarters'           => 'fa fa-thermometer-three-quarters',
        'fa fa-thumb-tack'                           => 'fa fa-thumb-tack',
        'fa fa-thumbs-down'                          => 'fa fa-thumbs-down',
        'fa fa-thumbs-o-down'                        => 'fa fa-thumbs-o-down',
        'fa fa-thumbs-o-up'                          => 'fa fa-thumbs-o-up',
        'fa fa-thumbs-up'                            => 'fa fa-thumbs-up',
        'fa fa-ticket'                               => 'fa fa-ticket',
        'fa fa-times'                                => 'fa fa-times',
        'fa fa-times-circle'                         => 'fa fa-times-circle',
        'fa fa-times-circle-o'                       => 'fa fa-times-circle-o',
        'fa fa-times-rectangle'                      => 'fa fa-times-rectangle',
        'fa fa-times-rectangle-o'                    => 'fa fa-times-rectangle-o',
        'fa fa-tint'                                 => 'fa fa-tint',
        'fa fa-toggle-down'                          => 'fa fa-toggle-down',
        'fa fa-toggle-left'                          => 'fa fa-toggle-left',
        'fa fa-toggle-off'                           => 'fa fa-toggle-off',
        'fa fa-toggle-on'                            => 'fa fa-toggle-on',
        'fa fa-toggle-right'                         => 'fa fa-toggle-right',
        'fa fa-toggle-up'                            => 'fa fa-toggle-up',
        'fa fa-trademark'                            => 'fa fa-trademark',
        'fa fa-train'                                => 'fa fa-train',
        'fa fa-transgender'                          => 'fa fa-transgender',
        'fa fa-transgender-alt'                      => 'fa fa-transgender-alt',
        'fa fa-trash'                                => 'fa fa-trash',
        'fa fa-trash-o'                              => 'fa fa-trash-o',
        'fa fa-tree'                                 => 'fa fa-tree',
        'fa fa-trello'                               => 'fa fa-trello',
        'fa fa-tripadvisor'                          => 'fa fa-tripadvisor',
        'fa fa-trophy'                               => 'fa fa-trophy',
        'fa fa-truck'                                => 'fa fa-truck',
        'fa fa-try'                                  => 'fa fa-try',
        'fa fa-tty'                                  => 'fa fa-tty',
        'fa fa-tv'                                   => 'fa fa-tv',
        'fa fa-tumblr'                               => 'fa fa-tumblr',
        'fa fa-tumblr-square'                        => 'fa fa-tumblr-square',
        'fa fa-turkish-lira'                         => 'fa fa-turkish-lira',
        'fa fa-twitch'                               => 'fa fa-twitch',
        'fa fa-twitter'                              => 'fa fa-twitter',
        'fa fa-twitter-square'                       => 'fa fa-twitter-square',
        'fa fa-venus'                                => 'fa fa-venus',
        'fa fa-venus-double'                         => 'fa fa-venus-double',
        'fa fa-venus-mars'                           => 'fa fa-venus-mars',
        'fa fa-viacoin'                              => 'fa fa-viacoin',
        'fa fa-viadeo'                               => 'fa fa-viadeo',
        'fa fa-viadeo-square'                        => 'fa fa-viadeo-square',
        'fa fa-video-camera'                         => 'fa fa-video-camera',
        'fa fa-vimeo'                                => 'fa fa-vimeo',
        'fa fa-vimeo-square'                         => 'fa fa-vimeo-square',
        'fa fa-vine'                                 => 'fa fa-vine',
        'fa fa-vk'                                   => 'fa fa-vk',
        'fa fa-volume-control-phone'                 => 'fa fa-volume-control-phone',
        'fa fa-volume-down'                          => 'fa fa-volume-down',
        'fa fa-volume-off'                           => 'fa fa-volume-off',
        'fa fa-volume-up'                            => 'fa fa-volume-up',
        'fa fa-umbrella'                             => 'fa fa-umbrella',
        'fa fa-underline'                            => 'fa fa-underline',
        'fa fa-undo'                                 => 'fa fa-undo',
        'fa fa-university'                           => 'fa fa-university',
        'fa fa-universal-access'                     => 'fa fa-universal-access',
        'fa fa-unlink'                               => 'fa fa-unlink',
        'fa fa-unlock'                               => 'fa fa-unlock',
        'fa fa-unlock-alt'                           => 'fa fa-unlock-alt',
        'fa fa-unsorted'                             => 'fa fa-unsorted',
        'fa fa-upload'                               => 'fa fa-upload',
        'fa fa-usb'                                  => 'fa fa-usb',
        'fa fa-usd'                                  => 'fa fa-usd',
        'fa fa-user'                                 => 'fa fa-user',
        'fa fa-user-circle'                          => 'fa fa-user-circle',
        'fa fa-user-circle-o'                        => 'fa fa-user-circle-o',
        'fa fa-user-md'                              => 'fa fa-user-md',
        'fa fa-user-o'                               => 'fa fa-user-o',
        'fa fa-user-plus'                            => 'fa fa-user-plus',
        'fa fa-user-secret'                          => 'fa fa-user-secret',
        'fa fa-user-times'                           => 'fa fa-user-times',
        'fa fa-users'                                => 'fa fa-users',
        'fa fa-vcard'                                => 'fa fa-vcard',
        'fa fa-vcard-o'                              => 'fa fa-vcard-o',
        'fa fa-warning'                              => 'fa fa-warning',
        'fa fa-wechat'                               => 'fa fa-wechat',
        'fa fa-weibo'                                => 'fa fa-weibo',
        'fa fa-weixin'                               => 'fa fa-weixin',
        'fa fa-whatsapp'                             => 'fa fa-whatsapp',
        'fa fa-wheelchair'                           => 'fa fa-wheelchair',
        'fa fa-wheelchair-alt'                       => 'fa fa-wheelchair-alt',
        'fa fa-wifi'                                 => 'fa fa-wifi',
        'fa fa-wikipedia-w'                          => 'fa fa-wikipedia-w',
        'fa fa-window-close'                         => 'fa fa-window-close',
        'fa fa-window-close-o'                       => 'fa fa-window-close-o',
        'fa fa-window-maximize'                      => 'fa fa-window-maximize',
        'fa fa-window-minimize'                      => 'fa fa-window-minimize',
        'fa fa-window-restore'                       => 'fa fa-window-restore',
        'fa fa-windows'                              => 'fa fa-windows',
        'fa fa-won'                                  => 'fa fa-won',
        'fa fa-wordpress'                            => 'fa fa-wordpress',
        'fa fa-wpbeginner'                           => 'fa fa-wpbeginner',
        'fa fa-wpexplorer'                           => 'fa fa-wpexplorer',
        'fa fa-wpforms'                              => 'fa fa-wpforms',
        'fa fa-wrench'                               => 'fa fa-wrench',
        'fa fa-xing'                                 => 'fa fa-xing',
        'fa fa-xing-square'                          => 'fa fa-xing-square',
        'fa fa-y-combinator'                         => 'fa fa-y-combinator',
        'fa fa-y-combinator-square'                  => 'fa fa-y-combinator-square',
        'fa fa-yahoo'                                => 'fa fa-yahoo',
        'fa fa-yc'                                   => 'fa fa-yc',
        'fa fa-yc-square'                            => 'fa fa-yc-square',
        'fa fa-yelp'                                 => 'fa fa-yelp',
        'fa fa-yen'                                  => 'fa fa-yen',
        'fa fa-yoast'                                => 'fa fa-yoast',
        'fa fa-youtube'                              => 'fa fa-youtube',
        'fa fa-youtube-play'                         => 'fa fa-youtube-play',
        'fa fa-youtube-square'                       => 'fa fa-youtube-square',
    ];
    return $cards;
}

function localizeDate($date)
{
    $locale = app()->getLocale();
    switch ($locale) {

        case 'ar':

            $smallDays           = [ "Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri" ];
            $arDays              = [ "السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة" ];
            $date                = str_replace($smallDays, $arDays, $date);

            $smallMonths         = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
            $arMonths            = [ "يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر" ];
            $date                = str_replace($smallMonths, $arMonths, $date);

            header('Content-Type: text/html; charset=utf-8');
            $standard_nums       = [ "0","1","2","3","4","5","6","7","8","9" ];
            $eastern_arabic_nums = [ "٠","١","٢","٣","٤","٥","٦","٧","٨","٩" ];
            $date                = str_replace($standard_nums , $eastern_arabic_nums , $date);

            $enAmPm              = [ "AM" , "PM" ];
            $arAmPm              = [ "ص" , "م" ];
            $date                = str_replace($enAmPm , $arAmPm , $date);

            return $date;
            break;

        default:
            return $date;
            break;
    }
}

function permission_checker($auth)
{
    if($auth->role->id == 1) return true;

    foreach($auth->role->permissions as $permission)
    {
        if(request()->route()->getName() != $permission->permission) continue;

        return true;
    }
    return false;
}

function permission_route_checker($route)
{
    $auth = auth()->guard('admin')->user();

    if($auth->role->id == 1) return true;

    foreach($auth->role->permissions as $permission)
    {
        if($route != $permission->permission) continue;

        return true;
    }
    return false;
}

function edit_permissions($permissions, $check)
{
    return in_array($check,$permissions->pluck('permission')->toArray()) ? 'checked' : '';
}

function getAllRoutes()
{
    $routes = Route::getRoutes();

    $arr = [];

    foreach ($routes as $key => $value)
    {
        $not_in = range(0, 14);

        if($value->getName() !== null && !in_array($key, $not_in))
        {
            if($value->getAction()['namespace'] === 'Front') continue;

            if($value->getAction()['namespace'] === 'Auth') continue;

            $prefix = explode('/', $value->getAction()['prefix'])[1];

            if($prefix == 'admin-panel')
            {
                $route = explode('.',$value->getAction()['as']);

                if(count($route) >= 2) $arr[$route[0]][$key] = $value->getAction()['as'];

                else $arr[$key] = $value->getAction()['as'];
            }
        }
    }
    return $arr;
}

function models($model = '')
{
    $models = [
        'role'              => ['icon' => 'stack2',       'color' => 'brown'],
        'admin'             => ['icon' => 'user-tie',     'color' => 'teal'],
        'setting'           => ['icon' => 'gear',         'color' => 'blue'],
//        'banner'            => ['icon' => 'megaphone',    'color' => 'green-300'],
        'user'              => ['icon' => 'users',        'color' => 'green'],
//        'gallery'           => ['icon' => 'images3',      'color' => 'slate'],
//        'championship'      => ['icon' => 'trophy2',      'color' => 'pink'],
//        'course'            => ['icon' => 'magazine',     'color' => 'indigo'],
//        'complaint'         => ['icon' => 'list',         'color' => 'orange'],
//        'tool'              => ['icon' => 'cash2',        'color' => 'violet-300'],
//        'project'           => ['icon' => 'microscope',   'color' => 'teal-300'],
//        'changesLog'        => ['icon' => 'file-empty',   'color' => 'pink-300'],
//        'accreditationLog'  => ['icon' => 'file-text',    'color' => 'indigo-300'],
//        'auditLog'          => ['icon' => 'file-stats2',  'color' => 'blue-300'],
//        'survey'            => ['icon' => 'file-text3',   'color' => 'violet'],
//        'agency'            => ['icon' => 'users2',       'color' => 'grey'],
//        'package'           => ['icon' => 'cash3',        'color' => 'grey-300'],
//        'bank'              => ['icon' => 'credit-card2', 'color' => 'teal-600'],
    ];

    return $model != '' ? $models[$model] : $models;
}

function GetDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

    return ($angle * $earthRadius) / 1000;
}

function apiResponse($data = [], $type = 'success', $pagination = null, $msg = '', $code = 200)
{
    if($type == 'success')
    {
        $response = ['data' => $data,'status' => true];

        $response['message'] = ($msg == '') ? trans('api.request-done-successfully') : $msg;

        if($pagination)
        {
            $response['last_page']    = $pagination['last_page'];
            $response['perPage']      = $pagination['perPage'];
            $response['current_page'] = (int)request()->page != 0 ? (int)request()->page : 1;
        }

        return response()->json($response, $code);
    }

    if($type == 'warning') return response()->json(['data' => $data,'status' => false, 'message' => $msg], $code);

    $response = ['data' => $data,'status' => false];

    $response['message'] = ($msg == '') ? trans('api.server_internal_error') : $msg;

    return response()->json($response, $code != 200 ? $code : 500);
}

function updateStatus($data, $model)
{
    // if the value comes with checked that mean we want the reverse value of it;
    return ($data['value'] == 'checked') ? $model->update(['status' => 0]) : $model->update(['status' => 1]);
}

function getModelCount($model, $withDeleted = false)
{
	if($withDeleted)
	{
		if($model == 'admin') return \App\Models\Admin::onlyTrashed()->where('role_id', '!=', 1)->count();
		$mo = "App\\Models\\".ucwords($model);
		return $mo::onlyTrashed()->count();
	}

	if($model == 'admin') return \App\Models\Admin::where('role_id', '<>', 1)->count();

	if($model == 'role') return \App\Models\Role::where('id', '<>', 1)->count();

	$mo = "App\\Models\\".ucwords($model);

	return $mo::count();
}

function save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
{
	foreach (config('sitelangs.locales') as $lang => $name)
	{
		foreach ($model->translatedAttributes as $attr) { $model->translateOrNew($lang)->$attr = $formTranslatedAttrs[$lang][$attr]; }
	}

	if ($otherAttrs != null) {
		$otherAttrs = arr()->except($otherAttrs, ['_method','_token'] + $excepted);
		foreach ($otherAttrs as $key => $value) { $model->$key = $value; }
	}
}

function load_translated_attrs($model)
{
	foreach (config('sitelangs.locales') as $lang => $name)
	{
		$model->$lang = [];
		foreach ($model->translatedAttributes as $attr) $model->$lang = $model->$lang + [$attr => $model->getTranslation($lang)->$attr];
	}
}

function pagination($page, $model, $perPage = 10)
{
	$start  = ($page == 1) ? 0 : ($page - 1) * $perPage;

	$paginatedModel = ($page == 0 ? $model : $model->slice($start, $perPage));

	$total = $model->count();

	$count_pages = (int)$total / (int)$perPage;

	$last_page =  $count_pages >= 1 ? (int)$count_pages : 1;

	$data['total'] 	   = $total;
	$data['paginated'] = $paginatedModel;
	$data['last_page'] = $last_page;
	$data['perPage']   = $perPage;

	return $data;
}

function push_notification($title, $body, $fcm_token, $type = '', $notificationType = 'dash', $duration = '', $message = '')
{
    return create_curl_notification([
        'to' => $fcm_token,
        'notification' => [
            'title'            => $title,
            'body'             => $body,
            'notificationType' => $notificationType,
            'type'             => $type,
            'duration'         => $duration,
            'message'          => $message,
            'sound'            => true
        ]
    ]);
}

function create_curl_notification(array $fcmNotification = [])
{
    return Http::withToken(getSetting('notification_key'))->post('https://fcm.googleapis.com/fcm/send', $fcmNotification);
}

function apiResponseWarning($message = '')
{
    return apiResponse(null,'warning',null, $message);
}

function apiResponseSuccess($message = '')
{
    return response()->json(['status' => true, 'message' => $message == '' ? trans('api.request-done-successfully') : $message]);
}

function apiResponseFails($message = '')
{
    return apiResponse(null, 'fails', null, $message == '' ? trans('api.server_internal_error') : $message);
}

function payTabsPayment($payment, $request)
{
	$data['merchant_email'] = $payment['email'];
	$data['secret_key']     = $payment['key'];
	$data['transaction_id'] = $request->transaction_id;
	$data['order_id']       = $request->order_id;

	$response = Http::withHeaders(['content-type: multipart/form-data'])->post('https://www.paytabs.com/apiv2/verify_payment_transaction', $data);

	$_res = [];

	switch ($response->response_code) {
	    case 4001:
	        $msg = 'Missing parameters.';
	        break;

	    case 4002:
	        $msg = 'Invalid Credentials.';
	        break;

	    case 4003:
	        $msg = 'There are no transactions available.';
	        break;

	    case 0404:
	        $msg = 'You don’t have permissions.';
	        break;

	    case 481:
	        $msg = 'This transaction may be suspicious, your bank holds for further confirmation. Payment Provider has rejected this transaction due to suspicious activity; Your bank will reverse the dedicated amount to your card as per their policy.';
	        break;

	    default:
	        $msg = 'Payment is completed Successfully.';
	        $_res = $response;
	        break;
	}

	$res['msg']    = $msg;
	$res['code']   = $response->response_code;
	$res['result'] = $_res;

	return $res;
}

function modelForceDelete($model, $id, $hasFCM = false)
{
    $text  = last(explode('\\', $model));

    $model_name = Str::lower($text);

    $image_folder = Str::plural($model_name);

    $currentModel = $model::onlyTrashed()->where('id', $id)->first();

    if(isset($currentModel->image))
    {
        File::delete('public/uploaded/'.$image_folder.'/' . $currentModel->image->image);

        $currentModel->image->delete();
    }

    if($hasFCM)
    {
        $currentModel->fcm()->delete();
        $currentModel->token()->delete();
    }

    return $currentModel->forceDelete();
}

function formCreateArray($table)
{
    return [
        'route' => $table.'.store',
        'method' => 'POST',
        'class' => 'form-horizontal push-10-t '.$table.' ajax create',
        'files' => true
    ];
}

function formUpdateArray($model, $table)
{
    return [
        'url'    => route($table.'.update', $model->id),
        'method' => 'PUT',
        'class'  => 'form-horizontal push-10-t '.$table.' ajax edit',
        'files'  => true,
    ];
}

function crudResponse($mess)
{
    return response()->json(['requestStatus' => true, 'message' => $mess]);
}

function crudResponseFails($mess)
{
    return response()->json(['requestStatus' => false, 'message' => $mess]);
}

function getRate($rates)
{
    $_rates = $rates->map(function($item, $key){ return (int)$item->rate; })->toArray();

    $count = count($_rates);

    $sum = array_sum($_rates);

    return $count != 0  ? floor((int)($sum / $count)) : 0;
}

function getSettingsByType(string $type)
{
    return \App\Models\Setting::select(['key', 'value'])->where('type', $type)->get();
}

function getSettingInCollection($collection, $key)
{
    return $collection->where('key', $key)->first()->value ?? trans('back.no-value');
}

function getContactUsPageData()
{
    $settings = getSettingsByType('CONTACTS');

    $site_location = explode(',', getSettingInCollection($settings, 'contact_site_location'));

    $data['lat'] = $site_location[0];

    $data['lng'] = $site_location[1];

    $data['contact_facebook'] = getSettingInCollection($settings, 'contact_facebook');

    $data['contact_instgram'] = getSettingInCollection($settings, 'contact_instgram');

    $data['contact_twitter'] = getSettingInCollection($settings, 'contact_twitter');

    $data['contact_phone'] = getSettingInCollection($settings, 'contact_phone');

    $data['contact_email'] = getSettingInCollection($settings, 'contact_email');

    $data['contact_site_address'] = getSettingInCollection($settings, 'contact_site_address');

    $data['contact_site_location'] = getSettingInCollection($settings, 'contact_site_location');

    return $data;
}

function getDefaultImage($model, $folder)
{
    return ($model->image != null) ? url('public/uploaded/'.$folder.'/' . $model->image->image) : defaultImage();
}

function getDefaultImages($model, $folder)
{
    return ($model->images != null) ? url('public/uploaded/'.$folder.'/' . $model->image->image) : defaultImage();
}

function crudRoutes($table, $controller, $withShow = false)
{
    $model = str()->singular($table);

    return Route::group(['prefix' => $table, 'as' => $table.'.'], function () use ($controller, $model, $withShow) {
        Route::post('/ajax-delete-'.$model, $controller.'@Delete'.ucfirst($model))->name('ajax-delete-'.$model);
        Route::post('/ajax-change-'.$model.'-status', $controller.'@ChangeStatus')->name('ajax-change-'.$model.'-status');
        if($withShow) Route::get('/show/{'.$model.'}', $controller.'@show'.ucfirst($model))->name('show-'.$model);
        Route::get('/export', $controller.'@export')->name('export');
        Route::get('/get/trashed', $controller.'@trashed')->name('trashed');
        Route::get('/restore/{id}/trashed', $controller.'@restore')->name('restore');
        Route::get('/delete/{id}/trashed', $controller.'@forceDelete')->name('delete');
    });
}

function defaultImage()
{
    return url('public/admin/img/avatar10.jpg');
}

function getCollectionBerMonthCount($collection)
{
    return [
        'jan' => $collection->filter(getTransfersByMonthName('January'))->first(),
        'feb' => $collection->filter(getTransfersByMonthName('February'))->first(),
        'mar' => $collection->filter(getTransfersByMonthName('March'))->first(),
        'apr' => $collection->filter(getTransfersByMonthName('April'))->first(),
        'may' => $collection->filter(getTransfersByMonthName('May'))->first(),
        'jun' => $collection->filter(getTransfersByMonthName('June'))->first(),
        'jul' => $collection->filter(getTransfersByMonthName('July'))->first(),
        'aug' => $collection->filter(getTransfersByMonthName('August'))->first(),
        'sep' => $collection->filter(getTransfersByMonthName('September'))->first(),
        'oct' => $collection->filter(getTransfersByMonthName('October'))->first(),
        'nov' => $collection->filter(getTransfersByMonthName('November'))->first(),
        'dec' => $collection->filter(getTransfersByMonthName('December'))->first(),
    ];
}

function getCollectionBerDayCount($collection)
{
    $data['saturday']  = $collection->filter(getUsersByDayName('Saturday'));
    $data['sunday']    = $collection->filter(getUsersByDayName('Sunday'));
    $data['monday']    = $collection->filter(getUsersByDayName('Monday'));
    $data['tuesday']   = $collection->filter(getUsersByDayName('Tuesday'));
    $data['wednesday'] = $collection->filter(getUsersByDayName('Wednesday'));
    $data['thursday']  = $collection->filter(getUsersByDayName('Thursday'));
    $data['friday']    = $collection->filter(getUsersByDayName('Friday'));

    return $data;
}

function getUsersByDayName($day) : callable
{
    return function ($item) use($day) {
        return $item->created_at->format('l') == $day;
    };
}

function getTransfersByMonthName($month) : callable
{
    return function ($item) use($month) {
        return $item->created_at->format('F') == $month;
    };
}

function curl_send_hisms_message($number, $message)
{
    $settings = \App\Models\Setting::where('type', 'SMS')->get();

    $sms_number = $settings->where('key', 'sms_number')->first()->value;

    $sms_password = $settings->where('key', 'sms_password')->first()->value;

    $sms_sender = $settings->where('key', 'sms_sender_name')->first()->value;

    $result = Http::withHeaders(['content-type: multipart/form-data'])->post('http://hisms.ws/api.php', [
        'send_sms' => '',
        'username' => $sms_number, // '966531927731'
        'password' => $sms_password, //'f123456kh',
        'numbers'  => $number,
        'sender'   => $sms_sender, // 'Toot'
        'message'  => $message,
    ]);

    return ['code' => $result, 'message' => get_message_by_code($result)];
}

function get_message_by_code($code)
{
    switch ($code)
    {
        case 1:
            return "إسم المستخدم غير صحيح";
            break;

        case 2:
            return "كلمة المرور غير صحيحة";
            break;

        case 404:
            return "لم يتم إدخال جميع البرامترات المطلوبة";
            break;

        case 403:
            return "تم تجاوز عدد المحاولات المطلوبة";
            break;

        case 504:
            return "الحساب معطل";
            break;

        case 4:
            return "لا يوجد أرقام";
            break;

        case 5:
            return "لا يوجد رسالة";
            break;

        case 6:
            return "سيندر خطئ";
            break;

        case 7:
            return "سيندر غير مفعل";
            break;

        case 8:
            return "الرسالة تحتوي كلمة ممنوعة";
            break;

        case 9:
            return "لا يوجد رصيد";
            break;

        case 10:
            return "صيغة التاريخ خاطئة";
            break;

        case 11:
            return "صيغة الوقت خاطئة";
            break;

        default:
            return "تم الإرسال";
    }
}

function getTranslatedEnglishCrud($model)
{
    $models = str()->plural($model);

    $crud["$models.ajax-delete-$model"]        = "Delete exist $model";
    $crud["$models.ajax-change-$model-status"] = "Change $model status";
    $crud["$models.index"]                     =  ucwords($models)." Page";
    $crud["$models.show-$model"]               = "Show $model";
    $crud["$models.create"]                    = "Create new $model page";
    $crud["$models.store"]                     = "Save new $model";
    $crud["$models.edit"]                      = "Edit $model";
    $crud["$models.update"]                    = "Update $model";
    $crud["$models.export"]                    = "Export $model table to excel file";
    $crud["$models.trashed"]                   = "Show trashed $models";
    $crud["$models.restore"]                   = "Restore a $model";
    $crud["$models.delete"]                    = "Remove $model from trash";

    return $crud;
}

function uploadPDF($request, $filename, $type = 'pdf')
{
    $name = null;

    if($request->hasFile($filename))
    {
        $ex = $request->$filename->getClientOriginalExtension();

        $name = $type . '_' . str()->random(12) . '_' . date('Y-m-d') .'.'. $ex;

        $request[$filename]->move('public/uploaded/files', $name);
    }

    return $name;
}

function getTaskStatus($task)
{
    if($task->actual_start_date->isCurrentDay()) return trans('back.started');

    if($task->actual_start_date->isFuture()) return trans('back.not-started');

    if ($task->actual_start_date->isPast() && $task->actual_end_date->isFuture()) return trans('back.still');

    if ($task->actual_start_date->isPast() && $task->actual_end_date->isPast()) return trans('back.finished');

    return 'Unknown';
}
