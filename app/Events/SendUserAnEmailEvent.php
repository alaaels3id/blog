<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendUserAnEmailEvent
{
    use Dispatchable, SerializesModels;

    public $message;
    public $email;

    public function __construct($message, $email)
    {
        $this->message = $message;
        $this->email = $email;
    }
}
