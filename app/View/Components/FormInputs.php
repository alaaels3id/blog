<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormInputs extends Component
{
    public $type;
    public $name;
    public $slug;

    public function __construct($type, $name, $slug)
    {
        $this->type = $type;
        $this->name = $name;
        $this->slug = $slug;
    }

    public function render()
    {
        return view('components.form-inputs');
    }
}
