<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PermissionItem extends Component
{
    public $role;
    public $value;
    public $key;

    public function __construct($role, $value, $key)
    {
        $this->role = $role;
        $this->value = $value;
        $this->key = $key;
    }

    public function render()
    {
        return view('components.permission-item');
    }
}
