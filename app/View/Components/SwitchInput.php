<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SwitchInput extends Component
{
    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function render()
    {
        return view('components.switch-input');
    }
}
