<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TrashMenu extends Component
{
    public $table;
    public $model;

    public function __construct($table, $model)
    {
        $this->table = $table;
        $this->model = $model;
    }

    public function render()
    {
        return view('components.trash-menu');
    }
}
