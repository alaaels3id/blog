<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PermissionListItem extends Component
{
    public $role;
    public $val;
    public $k;

    public function __construct($role, $val, $k)
    {
        $this->role = $role;
        $this->val = $val;
        $this->k = $k;
    }

    public function render()
    {
        return view('components.permission-list-item');
    }
}
