<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PageShowHeader extends Component
{
    public $model;
    public $title;

    public function __construct($model, $title)
    {
        $this->model = $model;
        $this->title = $title;
    }

    public function render()
    {
        return view('components.page-show-header');
    }
}
