<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MenuListItem extends Component
{
    public $models;
    public $trans;
    public $type;

    public function __construct($models, $type, $trans)
    {
        $this->models = $models;
        $this->type   = $type;
        $this->trans  = $trans;
    }

    public function render()
    {
        return view('components.menu-list-item');
    }
}
