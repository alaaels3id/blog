<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SideBarMenuItem extends Component
{
    public $table;
    public $icon;

    public function __construct($table, $icon)
    {
        $this->table = $table;
        $this->icon = $icon;
    }

    public function render()
    {
        return view('components.side-bar-menu-item');
    }
}
