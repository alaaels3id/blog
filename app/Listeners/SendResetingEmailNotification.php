<?php

namespace App\Listeners;

use App\Events\EmailResting;
use App\Mail\SendResetingMail;
use Illuminate\Support\Facades\Mail;

class SendResetingEmailNotification
{
    public function handle(EmailResting $event)
    {
        Mail::to($event->email)->send(new SendResetingMail($event->token));
    }
}
