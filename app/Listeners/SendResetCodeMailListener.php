<?php

namespace App\Listeners;

use App\Events\SendResetCodeMailEvent;
use App\Mail\SendResetCodeMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendResetCodeMailListener
{
    use InteractsWithQueue;

    public function handle(SendResetCodeMailEvent $event)
    {
        Mail::to($event->user->email)->send(new SendResetCodeMail($event->user, $event->message));
    }
}
