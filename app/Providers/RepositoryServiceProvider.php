<?php

namespace App\Providers;

use App\Repository\Eloquent\Sql\BaseRepository;
use App\Repository\Contracts\IEloquentRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        foreach (self::getModels('app/Models') as $model) {
            $this->app->bind('App\Repository\Contracts\\I'.$model.'Repository','App\Repository\Eloquent\Sql\\'.$model.'Repository');
        }
        $this->app->bind(IEloquentRepository::class, BaseRepository::class);
    }

    public function boot()
    {
        //
    }

    protected static function getModels($path)
    {
        $out = [];

        $results = scandir($path);

        foreach ($results as $result)
        {
            if ($result === '.' or $result === '..' or $result === 'Token.php' or $result === 'Fcm.php' or $result === 'Image.php') continue;

            $filename = $path . '/' . $result;

            if(Str::contains($result, 'Translation')) continue;

            if (is_dir($filename)) $out = array_merge($out, self::getModels($filename));

            else $out[] = substr($result,0,-4);
        }

        return $out;
    }
}
