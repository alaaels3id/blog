<?php

namespace App\Providers;

use App\Http\Callbacks\AppCallbacks;
use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    use AppCallbacks;

    public function register()
    {
        View::composer('*', self::getAuth());
    }

    public function boot()
    {
        View::composer('*', self::getAllRoutes());

        Blade::if('hasPermission', self::getHasPermission());

        $this->app->singleton('Crud', self::getCrudFacade());

        $this->app->singleton('Api', self::getApiFacade());

        User::observe(UserObserver::class);
    }
}
