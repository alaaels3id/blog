<?php


namespace App\Traits;


use App\Crud\Api;
use App\Http\Resources\UserResource;
use App\Models\User;

trait UserApi
{
    public static function apiLogin($request)
    {
        return Api::apiLogin(User::class,UserResource::class, $request);
    }

    public static function apiRegister($request)
    {
        return Api::apiRegister(User::class,UserResource::class, $request);
    }

    public static function apiSendActiveCode($request)
    {
        return Api::apiSendActiveCode(User::class, $request);
    }

    public static function apiForgetPassword($request)
    {
        return Api::apiForgetPassword(User::class, $request);
    }

    public static function apiCheckResetCode($request)
    {
        return Api::apiCheckResetCode(User::class, $request);
    }

    public static function apiResendResetCode($request)
    {
        return Api::apiResendResetCode(User::class, $request);
    }

    public static function apiChangePassword($request)
    {
        return Api::apiChangePassword(User::class, $request);
    }

    public static function apiSetNewPassword($request)
    {
        return Api::apiSetNewPassword(User::class,$request);
    }

    public static function apiUpdateUserProfile($request)
    {
        return Api::apiUpdateUserProfile($request);
    }
}
