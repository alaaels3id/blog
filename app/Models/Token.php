<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $guarded = ['id'];

    public function tokenable()
    {
        return $this->morphTo();
    }

    public static function createToken($createdModel, $jwt, $type)
    {
        return Token::create(['tokenable_id' => $createdModel->id, 'tokenable_type' => $type, 'jwt' => $jwt,'ip' => request()->ip()]);
    }
}
