<?php

namespace App\Models;

use App\Crud\Crud;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    use Translatable, SoftDeletes;

    public $translationModel = 'App\Models\FaqTranslation';

    public $translationForeignKey = 'faq_id';

    public $translatedAttributes = ['question', 'answer'];

    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function scopeStatue($query, $value = 1)
    {
        return $query->where('status', $value);
    }

    public static function changeStatus($request)
    {
        return Crud::setStatus(self::class, $request);
    }

    public static function createFaq($request)
    {
        try
        {
            $faqData = $request->all();

            $formTranslatedAttrs = arr()->only($faqData, array_keys(config('sitelangs.locales')));

            $faqData = arr()->except($faqData, array_keys(config('sitelangs.locales')));

            $faq = new Faq(arr()->except($faqData, ['_token']));

            save_translated_attrs($faq, $formTranslatedAttrs);

            $faq->save();

            return crudResponse(translated('add', 'faq'));
        }
        catch (Exception $e)
        {
            return crudResponseFails($e->getMessage());
        }
    }

    public static function updateFaq($request, $currentFaq)
    {
        try
        {
            $faqData = $request->all();

            $formTranslatedAttrs = arr()->only($faqData, array_keys(config('sitelangs.locales')));

            $faqData = arr()->except($faqData, array_keys(config('sitelangs.locales')));

            save_translated_attrs($currentFaq, $formTranslatedAttrs, $faqData);

            $currentFaq->save();

            return crudResponse(translated('edit', 'faq'));
        }
        catch (Exception $e)
        {
            return crudResponseFails($e->getMessage());
        }
    }

    public static function deleteFaq($request)
    {
        if (!$currentFaq = Faq::find($request->id)) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Faq is not exists !!']);

        try
        {
            $currentFaq->delete();

            return response()->json(['deleteStatus' => true, 'message' => translated('delete', 'faq')]);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false, 'error' => $e->getMessage()]);
        }
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $faqs   = [];

        $with_null == null ? $faqs = $faqs : $faqs = $faqs + [''  => ''];
        $with_main == null ? $faqs = $faqs : $faqs = $faqs + ['0' => 'Main'];

        $faqsDB = Faq::whereNotIn('id',$exceptedIds)->get();

        foreach ($faqsDB as $faq)
        {
            $faqs[$faq->id] = ucwords($faq->name);
        }

        return $faqs;
    }
}
