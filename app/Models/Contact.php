<?php

namespace App\Models;

use App\Crud\Crud;
use App\Events\SendUserAnEmailEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Exception;

class Contact extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['diff_for_humans', 'last_update'];

    public function getDiffForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getLastUpdateAttribute()
    {
        return $this->updated_at->diffForHumans();
    }

    public static function deleteMessage($request)
    {
        return Crud::delete(Contact::class, $request);
    }

    public static function apiCreateContactMessage($request)
    {
        try
        {
            Contact::create(self::setData($request));

            return apiResponseSuccess();
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    public static function sendUserReplayMessage(Request $request)
    {
        try
        {
            event(new SendUserAnEmailEvent($request->message, $request->email));
            return back()->with('success', 'تم ارسال الرسالة بنجاح');
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }

    private static function setData($request)
    {
        return [
            'name'    => $request->name ?? null,
            'phone'   => $request->phone ?? null,
            'title'   => $request->title,
            'message' => $request->message,
        ];
    }
}
