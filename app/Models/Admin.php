<?php

namespace App\Models;

use App\Crud\Crud;
use App\Events\SendAdminMailEvent;
use Exception;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes;

    public $guard = 'admin';

    protected $guarded = ['id', 'password_confirmation'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id','id')->withDefault(['name' => trans('no-value')]);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getImageUrlAttribute()
    {
        return ($this->image != null) ? url('public/uploaded/admins/' . $this->image->image) : url('public/admin/img/avatar10.jpg');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function createAdmin($request)
    {
        return Crud::store(Admin::class, $request, true);
    }

    public static function updateAdmin($request, $currentAdmin)
    {
        return Crud::update(Admin::class, $request, $currentAdmin, true);
    }

    public static function deleteAdmin($request)
    {
        return Crud::delete(Admin::class, $request->id);
    }

    public static function changeStatus($request)
    {
        return Crud::setStatus(Admin::class, $request);
    }

    public static function sendAdminMessage($request)
    {
        try
        {
            event(new SendAdminMailEvent($request->message, $request->email));

            return back()->with('success', trans('api.request-done-successfully'));
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }
}
