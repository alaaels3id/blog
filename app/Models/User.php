<?php

namespace App\Models;

use App\Crud\Crud;
use App\Traits\UserApi;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, UserApi;

    protected $guarded = ['id'];

    protected $hidden = ['password','remember_token'];

    protected $casts = ['email_verified_at' => 'datetime', 'status' => 'boolean'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getImageUrlAttribute()
    {
        return ($this->image != null) ? url('public/uploaded/users/' . $this->image->image) : url('public/admin/img/avatar10.jpg');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function token()
    {
        return $this->morphOne(Token::class, 'tokenable');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function fcm()
    {
        return $this->morphOne(Fcm::class,'fcmable');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getInSelectForm()
    {
        return Crud::getModelsInSelectedForm(self::class,'name');
    }
}
