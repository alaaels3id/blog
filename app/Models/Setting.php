<?php

namespace App\Models;

use App\Crud\Crud;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Setting extends Model
{
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function createSetting($request)
    {
        $settingData = $request->all();

        try
        {
            if($settingData['input'] == 'file') $settingData['value'] = uploaded($settingData['value'], 'setting');

            Setting::updateOrCreate(arr()->except($settingData, ['_token']));

            return response()->json(['requestStatus' => true, 'message' => translated('add', 'setting')]);
        }
        catch (Exception $e)
        {
            return response()->json(['requestStatus' => false, 'message' => $e->getMessage()]);
        }
    }

    public static function updateSetting($request, $currentSetting)
    {
        return Crud::update(Setting::class, $request, $currentSetting);
    }

    public static function deleteSetting($request)
    {
        return Crud::delete(Setting::class, $request->id);
    }

    public static function updateAll($request)
    {
        try
        {
            foreach ($request->except(['_token', 'submit']) as $key => $value)
            {
                $setting = Setting::where('key', $key)->first();

                if (is_file($value))
                {
                    File::delete('public/uploaded/settings/' . $setting->value);

                    $setting->update(['value' => uploaded($value, 'setting')]);
                }
                else
                {
                    $setting->update(['value' => $value]);
                }
            }
            return response()->json(['requestStatus' => true, 'message' => translated('edit', 'setting')]);
        }
        catch (Exception $e)
        {
            return response()->json(['requestStatus' => false, 'message' => $e->getMessage()]);
        }
    }
}
