<?php

namespace App\Models;

use App\Crud\Crud;
use Astrotomic\Translatable\Translatable;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $guarded = ['id'];

    public $translationModel = RoleTranslation::class;

    public $translationForeignKey = 'role_id';

    public $translatedAttributes = ['name'];

    protected $casts = ['status' => 'boolean'];

    public function permissions()
    {
        return $this->hasMany(Permission::class,'role_id','id');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $roles   = [];

        $with_null == null ? $roles = $roles : $roles = $roles + [''  => ''];
        $with_main == null ? $roles = $roles : $roles = $roles + ['0' => 'Main'];

        $rolesDB = Role::whereNotIn('id',$exceptedIds)->where('id','!=',1)->get();

        foreach ($rolesDB as $role) { $roles[$role->id] = ucwords($role->name); }

        return $roles;
    }
}
