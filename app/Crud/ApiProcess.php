<?php

namespace App\Crud;

use App\Events\EmailResting;
use App\Http\Resources\UserResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ApiProcess
{
    /**
     * @param $model
     * @param $resource
     * @param $request
     * @param $guard
     * @return JsonResponse
     */
    public function apiLogin($model, $resource, $request, $guard = 'api')
    {
        $user = $model::where(self::loginChecker($request))->first();

        if($user && !$user->status) return apiResponseWarning('عفوا هذا الحساب غير نشط حاليا برجاء تفعيل حسابك اولا');

        $type = preg_match('/^(\w*)@(gmail|outlook|yahoo)\.com$/i', $request->email) ? 'email' : 'phone';

        if (!$token = Auth::guard($guard)->attempt([$type => $request->email, 'password' => $request->password, 'status' => 1]))
        {
            return apiResponse(null,'fails',null, trans('auth.failed'), 203);
        }

        if($request->has('fcm_token')) auth()->guard($guard)->user()->fcm->update(['fcm' => $request->fcm_token]);

        auth()->guard($guard)->user()->token->update(['jwt' => $token, 'ip' => $request->ip()]);

        return apiResponse(new $resource(auth()->guard($guard)->user()),'success');
    }

    /**
     * @param $model
     * @param $resource
     * @param $request
     * @param array $other
     * @return JsonResponse
     */
    public function apiRegister($model, $resource, $request, $other = [])
    {
        $data = $request->all();

        if(!empty($other)) $data = $data + $other;

        $model_name = Str::of(last(explode('\\', $model)))->lower();

        try
        {
            if($request->hasFile('image')) $data['image'] = uploaded($request->image, $model_name);

            else $data = Arr::except($data, ['image']);

            $created = $model::create($data);

            if($request->hasFile('image')) $created->image()->create(['image' => $data['image']]);

            return apiResponse(new $resource($created), 'success');
        }
        catch(Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @param $resource
     * @param bool $imageModel
     * @return JsonResponse
     */
    public function apiStore($model, $request, $resource = null, $imageModel = false)
    {
        $data = !is_array($request) ? $request->all() : $request;

        $modelName = Str::lower(last(explode('\\', $model)));

        $image = null;

        try
        {
            if (request()->hasFile('image')) $image = uploaded($data['image'], $modelName);

            else $data = Arr::except($data, ['image']);

            $created = $model::create(Arr::except($data, ['_token', 'password_confirmation', 'image']));

            if(request()->hasFile('image') && $imageModel) $created->image()->create(['image' => $image]);

            if(!$resource) return apiResponseSuccess();

            return apiResponse(new $resource($created),'success');
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiSendActiveCode($model, $request)
    {
        try
        {
            $code = create_rand_numbers(4);

            $checkEmail = $model::where('email',$request->email)->first();

            if(!$checkEmail) return apiResponse([], 'success',null,'عفوا البريد الالكتروني غير موجود');

            return apiResponse(['active_code' => $code],'success',null,'تم ارسال كود التفعيل بنجاح');
        }
        catch(Exception $e)
        {
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiForgetPassword($model, $request)
    {
        try
        {
            $userCheckPasswordReset = DB::table('password_resets')->whereEmail($request->email)->get()->toArray();

            $user = $model::whereEmail($request->email)->first();

            $token = create_rand_numbers();

            if (count($userCheckPasswordReset) == 0) {
                if (!DB::table('password_resets')->insert(['email' => $request->email, 'token' => Hash::make($token), 'created_at' => now()])) return apiResponse([], 'fails');
            }

            DB::table('password_resets')->whereEmail($request->email)->update(['password_resets.token' => Hash::make($token)]);

            $user->update(['code' => $token]);

            event(new EmailResting($token, $request->email));

            return apiResponse([], 'success', null, trans('api.mail-send-successfully'));
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiResetPassword($model, $request)
    {
        try
        {
            $userCheckPasswordReset = DB::table('password_resets')->whereEmail($request->email)->first();

            $user = $model::where('email', $request->email)->where('status', 1)->first();

            if ($userCheckPasswordReset && Hash::check($request->code, $userCheckPasswordReset->token))
            {
                if (!$user->update(['password' => $request->password]))
                    return apiResponse([], 'fails', null, trans('api.server_internal_error'));

                DB::table('password_resets')->whereEmail($request->email)->delete();
            }
            else
            {
                return apiResponse([], 'fails', null, trans('api.the-code-is-invalid'));
            }

            return apiResponseSuccess();
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiChangePassword($model, $request)
    {
        try
        {
            $user = $model::where('id', $request->user()->id)->whereStatus(1)->first();

            if (!Hash::check($request['old_password'], $user->password)) return apiResponse([], 'warning', null, trans('api.password-is-not-matched'));

            if (!$user) return apiResponse([], 'warning', null, trans('api.user-is-not-found-or-not-actived'));

            $user->update(['password' => $request['password']]);

            return apiResponse([], 'success', null, trans('api.request-done-successfully'));
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiCheckResetCode($model, $request)
    {
        try
        {
            $user = $model::where('email', $request->email)->where('code', $request->code)->first();

            if(!$user) return apiResponse([], 'warning',null,'عفوا هذا الكود غير صحيح');

            $user->update(['code' => null]);

            return apiResponse([],'success',null,'تم التأكد من صحة الكود');
        }
        catch(Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiSetNewPassword($model, $request)
    {
        try
        {
            $user = $model::where('email', $request->email)->where('status', 1)->first();

            if (!$user->update(['password' => $request['password']])) return apiResponseSuccess(trans('api.server_internal_error'));

            return apiResponseSuccess();
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function apiResendResetCode($model, $request)
    {
        try
        {
            $user = $model::whereEmail($request->email)->first();

            $token = create_rand_numbers();

            $user->update(['code' => $token]);

            event(new EmailResting($token, $request->email));

            return apiResponse([], 'success', null, trans('api.mail-send-successfully'));
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $request
     * @return JsonResponse
     */
    public static function apiUpdateUserProfile($request)
    {
        try
        {
            $data = $request->all();

            $userImage = $request->user()->image ? $request->user()->image->image : null;

            $user = auth()->guard('api')->user();

            if ($request->hasFile('image'))
            {
                if(!$userImage) $user->image()->create(['image' => null]);

                File::delete('public/uploaded/users/' . $userImage);

                $user->image()->update(['image' => uploaded($data['image'], 'user')]);
            }

            else $data = Arr::except($data, ['image']);

            $user->update(Arr::except($data, ['image']));

            return apiResponse(new UserResource($user), 'success');
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $request
     * @return callable
     */
    private static function loginChecker($request) : callable
    {
        return function($query) use ($request) {
            $checkIsEmail = preg_match('/^(\w*)@(gmail|outlook|yahoo)\.com$/i', $request->email);
            return $checkIsEmail ? $query->where('email', $request->email) : $query->where('phone', $request->email);
        };
    }
}
