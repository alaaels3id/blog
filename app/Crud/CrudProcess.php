<?php

namespace App\Crud;

use App\Events\SendUserAnEmailEvent;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CrudProcess
{
    /**
     * @param $model
     * @param $request
     * @param string $fileName
     * @param bool $imageModel
     * @return JsonResponse
     */
    public function store($model, $request, $fileName = 'image', $imageModel = false)
    {
        $data = !is_array($request) ? $request->all() : $request;

        $model_name = Str::lower(last(explode('\\', $model)));

        $image = null;

        try
        {
            if (isset($data[$fileName])) $image = uploaded($data[$fileName], $model_name);

            $created = $model::create(Arr::except($data, ['_token', 'password_confirmation', (string)$fileName]));

            if(isset($data[$fileName]) && $imageModel) $created->image()->create(['image' => $image]);

            return crudResponse(translated('add', $model_name));
        }
        catch (Exception $e)
        {
            return crudResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $request
     * @param $currentModel
     * @param $fileName
     * @param bool $imageModel
     * @return JsonResponse
     */
    public function update($model, $request, $currentModel, $fileName = 'image', $imageModel = false)
    {
        $model_name = Str::of(last(explode('\\', $model)))->lower();

        $image_folder = $model_name->plural();

        $data = !is_array($request) ? $request->all() : $request;

        $image = null;

        $currentModelImage = $currentModel->image ? $currentModel->image->image : '';

        try
        {
            if (isset($data[$fileName]) && $imageModel)
            {
                File::delete('public/uploaded/'.$image_folder.'/'.$currentModelImage);
                $image = uploaded($data[$fileName], $model_name);
            }

            else $data = Arr::except($data, ['_method', '_token']);

            if(isset($data[$fileName]) && $imageModel) self::setOrUpdateModelImage($currentModel, $image);

            $currentModel->update($data);

            return response()->json(['requestStatus' => true, 'message' => translated('edit', lcfirst($model_name))]);
        }
        catch (Exception $e)
        {
            return response()->json(['requestStatus' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @param $model
     * @param $id
     * @return JsonResponse
     */
    public function delete($model, $id)
    {
        $model_name = Str::of(last(explode('\\', $model)))->lower();

        $currentModel = $model::findOrFail($id);

        if (!$currentModel) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, '.$model_name.' is not exists !!']);

        try
        {
            $currentModel->delete();

            return response()->json(['deleteStatus' => true, 'message' => translated('delete', lcfirst(last(explode('\\', $model))))]);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false, 'error' => $e->getMessage()]);
        }
    }

    /**
     * @param $model
     * @param $request
     * @return JsonResponse
     */
    public static function setStatus($model, $request)
    {
        $model_name = lcfirst(last(explode('\\', $model)));

        if (!$updateStatus = updateStatus($request->all(), $model::find($request->id)))
            return response()->json(['requestStatus' => false, 'message' => trans('api.server-internal-error')]);

        return response()->json(['requestStatus' => true, 'message' => translated('edit', $model_name)]);
    }

    /**
     * @param $model
     * @param $request
     * @param bool $imageModel
     * @return JsonResponse
     */
    public static function storeTranslatedModel($model, $request, $imageModel = false)
    {
        try
        {
            $modelData = !is_array($request) ? $request->all() : $request;

            $model_name = Str::lower(last(explode('\\', $model)));

            $modelData = Arr::except($modelData, ['image']);

            $formTranslatedAttrs = arr()->only($modelData, array_keys(config('sitelangs.locales')));

            $modelData = arr()->except($modelData, array_keys(config('sitelangs.locales')));

            $object = new $model(arr()->except($modelData, ['_token']));

            save_translated_attrs($object, $formTranslatedAttrs);

            $object->save();

            if(request()->hasFile('image') && $imageModel) $object->image()->create(['image' => uploaded(request()->image, $model_name)]);

            return crudResponse(translated('add', lcfirst(last(explode('\\', $model)))));
        }
        catch (Exception $e)
        {
            return crudResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $currentModel
     * @param $request
     * @param bool $imageModel
     * @return JsonResponse
     */
    public static function updateTranslatedModel($model, $request, $currentModel, $imageModel = false)
    {
        try
        {
            $modelData = !is_array($request) ? $request->all() : $request;

            $model_name = Str::lower(last(explode('\\', $model)));

            $model_folder = Str::plural($model_name);

            $modelData = Arr::except($modelData, ['image']);

            $image = null;

            $currentModelImage = $currentModel->image ? $currentModel->image->image : '';

            if(request()->hasFile('image'))
            {
                File::delete('public/uploaded/'.$model_folder.'/' . $currentModelImage);

                $image = uploaded(request()->image, $model_name);
            }

            $formTranslatedAttrs = arr()->only($modelData, array_keys(config('sitelangs.locales')));

            $modelData = arr()->except($modelData, array_keys(config('sitelangs.locales')));

            save_translated_attrs($currentModel, $formTranslatedAttrs, $modelData);

            if(request()->hasFile('image') && $imageModel) CrudProcess::setOrUpdateModelImage($currentModel, $image);

            $currentModel->save();

            return crudResponse(translated('edit', lcfirst(last(explode('\\', $model)))));
        }
        catch (Exception $e)
        {
            return crudResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $image
     * @return mixed
     */
    public static function setOrUpdateModelImage($model, $image)
    {
        return ($model->image) ? $model->image()->update(['image' => $image]) : $model->image()->create(['image' => $image]);
    }

    /**
     * @param $model
     * @param $name
     * @param array $exceptedIds
     * @return array
     */
    public static function getModelsInSelectedForm($model, $name, $exceptedIds = [])
    {
        $list   = [];

        $modelsDB = $model::whereNotIn('id',$exceptedIds)->where('status', 1)->get();

        foreach ($modelsDB as $modelDB)
        {
            $list[$modelDB->id] = ucwords($modelDB->$name);
        }

        return $list;
    }

    /**
     * @param $request
     * @return RedirectResponse
     */
    public static function sendUserMessage($request)
    {
        try
        {
            event(new SendUserAnEmailEvent($request->message, $request->email));

            return back()->with('success', trans('api.request-done-successfully'));
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * @param $request
     * @return RedirectResponse
     */
    public static function sendUserNotification($request)
    {
        try
        {
            $user = User::find($request->user_id);

            $user->notifications()->create(['title' => $request->title, 'body' => $request->body]);

            return back()->with('success', trans('api.request-done-successfully'));
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }
}
