<?php

namespace App\Crud;

use Illuminate\Support\Facades\Facade;

/**
 * @method static updateTranslatedModel(string $class, $currentModel, $request, $imageModel = false)
 * @method static storeTranslatedModel(string $class, $request, $imageModel = false)
 * @method static setStatus(string $class, $request)
 * @method static store(string $class, $request, string $fileName = 'image', bool $imageModel = false)
 * @method static update(string $class, $request, $currentModel, string $fileName = 'image', bool $imageModel = false)
 * @method static delete(string $class, $id)
 * @method static getModelsInSelectedForm($model, $name, array $exceptedIds = [])
 * @method static sendUserMessage($request)
 * @method static sendUserNotification($request)
 */

class Crud extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Crud';
    }
}
