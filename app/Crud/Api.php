<?php


namespace App\Crud;

use Illuminate\Support\Facades\Facade;

/**
 * @method static apiLogin(string $class, $resource, $request, $guard = 'api')
 * @method static apiRegister(string $class, $resource, $request, $other = [])
 * @method static apiStore(string $class, $request, $resource = null, $imageModel = false)
 * @method static apiSendActiveCode(string $class, $request)
 * @method static apiForgetPassword(string $class, $request)
 * @method static apiResetPassword(string $class, $request)
 * @method static apiChangePassword(string $class, $request)
 * @method static apiSetNewPassword(string $class, $request)
 * @method static apiCheckResetCode(string $class, $request)
 * @method static apiResendResetCode(string $class, $request)
 * @method static apiUpdateUserProfile($request)
 */
class Api extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Api';
    }
}
