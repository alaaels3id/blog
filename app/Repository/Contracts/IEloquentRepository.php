<?php

namespace App\Repository\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface IEloquentRepository
{
    /**
     * @return mixed
     */
    public function index();
    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model;

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param int $pages
     * @return mixed
     */
    public function paginate($pages = 10);

    /**
     * @return mixed
     */
    public function export();

    /**
     * @param $id
     * @return mixed
     */
    public function restore($id);

    /**
     * @return mixed
     */
    public function trashed();

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
