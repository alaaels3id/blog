<?php

namespace App\Repository\Contracts;

use Illuminate\Http\Request;

/**
 * @method all()
 * @method paginate()
 * @method find($id)
 * @method delete($id)
 * @method index()
 */
interface IRoleRepository
{
    /**
     * @return mixed
     */
    public function apiIndex();

    /**
     * @param $id
     * @return mixed
     */
    public function forceDelete($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request);

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request);

    /**
     * @param Request $request
     * @param $currentModel
     * @return mixed
     */
    public function update(Request $request, $currentModel);
}
