<?php

namespace App\Repository\Eloquent\Sql;

use App\Crud\Crud;
use App\Exports\UsersExport;
use App\Http\Callbacks\UserCallbacks;
use App\Models\User;
use App\Repository\Contracts\IUserRepository;
use Exception;
use Illuminate\Http\Request;

class UserRepository extends BaseRepository implements IUserRepository
{
    use UserCallbacks;

    protected $model;

    protected $class;

    /**
     * IUserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;

        $this->class = User::class;

        parent::__construct($model, new UsersExport());
    }

    /**
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function apiIndex()
    {
        try
        {
            return dataTables()->of($this->model::get())
                ->editColumn('updated_at', self::getEditedUpdatedAt())
                ->editColumn('since', self::getEditedSince())
                ->make(true);
        }
        catch (Exception $e)
        {
            return response()->json(['error' => $e->getMessage()],500);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return Crud::store($this->modelPath, $request,'image', true);
    }

    /**
     * @param Request $request
     * @param $currentModel
     * @return mixed
     */
    public function update(Request $request, $currentModel)
    {
        return Crud::update($this->class, $request, $currentModel, 'image', true);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function forceDelete($id)
    {
        modelForceDelete($this->class, $id, true);

        return back()->with('success', translated('remove','user'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed|string
     */
    public function showUserMessage($id)
    {
        if (!$user = $this->model::find($id)) return response()->json(['requestStatus' => false, 'message' => trans('responseMessages.product-not-exist')]);

        return view('Back.Users.sendUserMessageModal', compact('user'))->render();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendUserMessage(Request $request)
    {
        return Crud::sendUserMessage($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendUserNotification(Request $request)
    {
        return Crud::sendUserNotification($request);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|mixed
     */
    public function create()
    {
        return view('Back.Crud.create', ['model' => 'user']);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|mixed
     */
    public function edit($id)
    {
        return view('Back.Crud.edit', ['currentModel' => $this->model::findOrFail($id), 'model' => 'user']);
    }
}
