<?php

namespace App\Repository\Eloquent\Sql;

use App\Repository\Contracts\IEloquentRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Crud\Crud;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class BaseRepository implements IEloquentRepository
{
    protected $model;

    protected $modelName;

    protected $modelPath;

    protected $export;

    public function __construct(Model $model, $export)
    {
        $this->model = $model;

        $this->export = $export;

        $this->modelPath = 'App\\Models\\'.Str::of($model->getTable())->singular()->ucfirst();

        $this->modelName = explode('\\', $this->modelPath)[2];
    }

    public function find($id): ?Model
    {
        return $this->model->findOrFail($id);
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginate($pages = 10)
    {
        return $this->model->paginate($pages);
    }

    public function export()
    {
        return Excel::download($this->export, str()->plural($this->modelName).'.xlsx');
    }

    public function restore($id)
    {
        $model = str()->of($this->modelName)->snake()->plural();

        DB::table($model)->where('id', $id)->update(['deleted_at' => null]);

        return back()->with('success', translated('restore', $model));
    }

    public function trashed()
    {
        return view('Back.'.ucfirst(str()->plural($this->modelName)).'.trashed', ['trashes' => $this->model::onlyTrashed()->get()]);
    }

    public function delete($id)
    {
        Crud::delete($this->modelPath, $id);
    }

    public function index()
    {
        $folder = Str::plural($this->modelName);

        return view('Back.'.$folder.'.index');
    }
}
