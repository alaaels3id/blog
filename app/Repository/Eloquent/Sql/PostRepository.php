<?php

namespace App\Repository\Eloquent\Sql;

use App\Models\Post;
use App\Repository\Contracts\IPostRepository;

class PostRepository extends BaseRepository implements IPostRepository
{
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }
}
