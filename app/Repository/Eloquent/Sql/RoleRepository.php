<?php

namespace App\Repository\Eloquent\Sql;

use App\Crud\Crud;
use App\Exports\RolesExport;
use App\Http\Callbacks\RoleCallbacks;
use App\Models\Role;
use App\Repository\Contracts\IRoleRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleRepository extends BaseRepository implements IRoleRepository
{
    use RoleCallbacks;

    protected $model;

    protected $class;

    /**
     * IUserRepository constructor.
     * @param Role $model
     */
    public function __construct(Role $model)
    {
        $this->model = $model;

        $this->class = Role::class;

        parent::__construct($model, new RolesExport());
    }

    public function apiIndex()
    {
        try
        {
            return dataTables()->of($this->model::orderBy('id', 'desc')->where(self::checkIfSuperAdmin())->get())
                ->editColumn('updated_at', self::getEditedUpdatedAt())
                ->editColumn('since', self::getEditedSince())
                ->make(true);
        }
        catch (Exception $e)
        {
            return response()->json(['error' => $e->getMessage()],500);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $createdRole = $this->model::create(['name' => $request->name, 'status' => $request->status]);

            $permissions = [];

            foreach ($request->permissions as $key => $permission)
            {
                if($permission == null) continue;

                $permissions[$key]['role_id'] = $createdRole->id;

                $permissions[$key]['permission'] = $permission;

                $permissions[$key]['created_at'] = now();

                $permissions[$key]['updated_at'] = now();
            }

            DB::table('permissions')->insert($permissions);

            DB::commit();

            return response()->json(['requestStatus' => true,'message' => translated('add','permission')]);
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response()->json(['requestStatus' => false,'message' => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @param $currentModel
     * @return mixed
     */
    public function update(Request $request, $currentModel)
    {
        DB::beginTransaction();
        try
        {
            $permissions = [];

            foreach ($request->permissions as $key => $permission)
            {
                if($permission == null) continue;

                $permissions[$key]['role_id']    = $currentModel->id;
                $permissions[$key]['created_at'] = now();
                $permissions[$key]['updated_at'] = now();
                $permissions[$key]['permission'] = $permission;
            }

            $currentModel->update(['name' => $request->name]);

            DB::table('permissions')->where('role_id', $currentModel->id)->delete();

            DB::table('permissions')->insert($permissions);

            DB::commit();

            return response()->json(['requestStatus' => true, 'message' => translated('edit','permission')],200);
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response()->json(['requestStatus' => false,'message' => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function forceDelete($id)
    {
        modelForceDelete($this->class, $id, true);

        return back()->with('success', translated('remove','user'));
    }

    public function create()
    {
        return view('Back.Crud.create', ['model' => 'role']);
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', ['currentModel' => $this->model::findOrFail($id), 'model' => 'user']);
    }
}
